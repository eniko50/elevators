package com.liftovi.data;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.domain.Page;
import org.springframework.test.context.junit4.SpringRunner;

import com.liftovi.model.Building;
import com.liftovi.model.Elevator;

@RunWith(SpringRunner.class)
@DataJpaTest
public class ElevatorRepositoryUnitTest {
	
	@Autowired
	ElevatorRepository elevatorRepository;
	
	@Autowired
	private TestEntityManager entityManager;
	
	@Test
	public void testFindAllByBuildingId() {
		Elevator elevator = new Elevator(null, 25, 3, 120, true, null);
		Building building = new Building(null, "kiki", 120, null);
		entityManager.persist(building);
		elevator.setBuilding(building);
		
		entityManager.persist(elevator);
		
		Page<Elevator> elevators = elevatorRepository.findAllByBuildingId(building.getId(), null);
		
		assertEquals(1, elevators.getContent().size());
		assertTrue(elevators.getContent().get(0).isWorking());
		assertEquals(25, elevators.getContent().get(0).getCode());
		assertEquals(3, elevators.getContent().get(0).getCurrentFloor());
		assertEquals(120, elevators.getContent().get(0).getFloorsLeft());
	}
	
	@Test
	public void testFindAllByBuildingIdAndWorkingTrue() {
		Elevator elevator = new Elevator(null, 25, 3, 120, true, null);
		Building building = new Building(null, "kiki", 120, null);
		entityManager.persist(building);
		elevator.setBuilding(building);
		
		entityManager.persist(elevator);
		
		List<Elevator> elevators = elevatorRepository.findAllByBuildingIdAndWorkingTrue(building.getId());
		
		assertEquals(1, elevators.size());
		assertTrue(elevators.get(0).isWorking());
		assertEquals(25, elevators.get(0).getCode());
		assertEquals(3, elevators.get(0).getCurrentFloor());
		assertEquals(120, elevators.get(0).getFloorsLeft());
	}
	
	@Test
	public void testFindAllByBuildingIdAndCode() {
		
		Elevator elevator = new Elevator(null, 25, 3, 120, true, null);
		Building building = new Building(null, "kiki", 120, null);
		entityManager.persist(building);
		elevator.setBuilding(building);
		
		entityManager.persist(elevator);
		
		Page<Elevator> elevators = elevatorRepository.findAllByBuildingIdAndCode(building.getId(), 25, null);
		
		assertEquals(1, elevators.getContent().size());
		assertTrue(elevators.getContent().get(0).isWorking());
		assertEquals(25, elevators.getContent().get(0).getCode());
		assertEquals(3, elevators.getContent().get(0).getCurrentFloor());
		assertEquals(120, elevators.getContent().get(0).getFloorsLeft());
	}
	
	@Test
	public void testAllByBuildingIdAndWorking() {
		
		Elevator elevator = new Elevator(null, 25, 3, 120, true, null);
		Building building = new Building(null, "kiki", 120, null);
		entityManager.persist(building);
		elevator.setBuilding(building);
		
		entityManager.persist(elevator);
		
		Page<Elevator> elevators = elevatorRepository.findAllByBuildingIdAndWorking(building.getId(), true, null);
		
		assertEquals(1, elevators.getContent().size());
		assertTrue(elevators.getContent().get(0).isWorking());
		assertEquals(25, elevators.getContent().get(0).getCode());
		assertEquals(3, elevators.getContent().get(0).getCurrentFloor());
		assertEquals(120, elevators.getContent().get(0).getFloorsLeft());
	}
	
	@Test
	public void testAllByBuildingIdAndCodeAndWorking() {
		
		Elevator elevator = new Elevator(null, 25, 3, 120, true, null);
		Building building = new Building(null, "kiki", 120, null);
		entityManager.persist(building);
		elevator.setBuilding(building);
		
		entityManager.persist(elevator);
		
		Page<Elevator> elevators = elevatorRepository.findAllByBuildingIdAndCodeAndWorking(building.getId(), 25, true, null);
		
		assertEquals(1, elevators.getContent().size());
		assertTrue(elevators.getContent().get(0).isWorking());
		assertEquals(25, elevators.getContent().get(0).getCode());
		assertEquals(3, elevators.getContent().get(0).getCurrentFloor());
		assertEquals(120, elevators.getContent().get(0).getFloorsLeft());
	}
		
}

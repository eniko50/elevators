package com.liftovi.data;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import com.liftovi.model.user.SecurityUser;

@RunWith(SpringRunner.class)
@DataJpaTest
public class UserRepositoryUnitTest {
	
	@Autowired
	private TestEntityManager entityManager;
	
	@Autowired
	UserRepository userRepository;
	
	@Test
	public void testFindByUsername() {
		SecurityUser user = new SecurityUser();
		user.setFirstName("Marko");
		user.setLastName("Markovic");
		user.setUsername("marko25");
		user.setPassword("12345");
		
		entityManager.persist(user);
		
		SecurityUser su = userRepository.findByUsername("marko25");
		
		assertEquals("Marko", su.getFirstName());
		assertEquals("Markovic", su.getLastName());
		assertEquals("marko25", su.getUsername());
		assertEquals("12345", su.getPassword());
	}
}

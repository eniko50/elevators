package com.liftovi.data;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import com.liftovi.model.user.SecurityAuthority;

@RunWith(SpringRunner.class)
@DataJpaTest
public class AuthorityRepositoryUnitTest {

	@Autowired
	private TestEntityManager entityManager;
	
	@Autowired
	AuthorityRepository authorityRepository;
	
	@Test
	public void testFindByName() {
		SecurityAuthority authority = new SecurityAuthority();
		authority.setName("ROLE_CUSTOM");
		entityManager.persist(authority);
		
		SecurityAuthority sa = authorityRepository.findByName("ROLE_CUSTOM");
		assertEquals("ROLE_CUSTOM", sa.getName());
	}
}

package com.liftovi.data;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.domain.Page;
import org.springframework.test.context.junit4.SpringRunner;

import com.liftovi.model.Building;

@RunWith(SpringRunner.class)
@DataJpaTest
public class BuildingRepositoryUnitTest {
	
	@Autowired
	private TestEntityManager entityManager;
	
	@Autowired
	BuildingRepository buildingRepository;
	
	@Before
	public void setUp() {
		entityManager.persist(new Building(null, "Kiki", 250,null));
		entityManager.persist(new Building(null, "Jeno", 120, null));
	}
	
	@Test
	public void testFindByNameContains() {
		
		Page<Building> buildings = buildingRepository.findByNameContains("Kik", null);
		
		assertEquals(1, buildings.getContent().size());
		assertEquals(250, buildings.getContent().get(0).getFloors());
		assertNull(buildings.getContent().get(0).getElevators());
	}
	
}

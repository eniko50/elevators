package com.liftovi.controller;

import static org.junit.Assert.assertEquals;

import java.util.HashSet;
import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.liftovi.data.BuildingRepository;
import com.liftovi.data.CommentRepository;
import com.liftovi.data.ElevatorRepository;
import com.liftovi.model.Building;
import com.liftovi.model.Comment;
import com.liftovi.model.Elevator;
import com.liftovi.service.BuildingService;
import com.liftovi.service.CommentService;
import com.liftovi.service.ElevatorService;
import com.liftovi.web.controller.UserController;
import com.liftovi.web.dto.BuildingDto;
import com.liftovi.web.dto.CommentDto;
import com.liftovi.web.dto.LoginDTO;
import com.liftovi.web.dto.TokenDTO;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class BuildingControllerTest {

	@Autowired
	private BuildingRepository buildingRepository;
	
	@Autowired
	private BuildingService buildingService;

	@Autowired
	private ElevatorRepository elevatorRepository;
	
	@Autowired
	private ElevatorService elevatorService;
	
	@Autowired
	private CommentRepository commentRepository;
	
	@Autowired
	private CommentService commentService;
	
	@Autowired
	private TestRestTemplate restTemplate;

	@Autowired
	private UserController userController;

	private TokenDTO token;

	@Before
	public void getToken() {
		ResponseEntity<TokenDTO> responseEntity = restTemplate.postForEntity("/api/login",
				new LoginDTO("admin", "12345"), TokenDTO.class);
		
		token = responseEntity.getBody();

		Building b1 = new Building(null, "Sheraton", 287, null);

		Elevator e11 = new Elevator(null, 25, 2, 125, true, b1);
		Elevator e12 = new Elevator(null, 45, 2, 12, true, b1);

		Set<Elevator> elevators1 = new HashSet<Elevator>();
		elevators1.add(e11);
		elevators1.add(e12);
		b1.setElevators(elevators1);

		buildingService.save(b1);
		elevatorService.save(e11);
		elevatorService.save(e12);

		Building b2 = new Building(null, "Promenada", 250, null);
		Elevator e21 = new Elevator(null, 89, 15, 0, false, b2);

		Set<Elevator> elevators2 = new HashSet<Elevator>();
		elevators2.add(e21);
		b2.setElevators(elevators2);
		buildingService.save(b2);
		elevatorService.save(e21);
		
		Comment c1 = new Comment(null, "petar", "lorem ipsum", 0, 1);
		Comment c2 = new Comment(null, "patra", "blablabla", 3, 0);
		commentService.save(c1);
		commentService.save(c2);
	}

	@After
	public void afterTests() {
		elevatorRepository.deleteAll();
		buildingRepository.deleteAll();
		commentRepository.deleteAll();
	}

	@Test
	public void testGetAllBuildingsPage() {

		final ResponseEntity<BuildingDto[]> responseWithoutQuery = restTemplate.getForEntity("/api/buildings",
				BuildingDto[].class);

		assertEquals(HttpStatus.OK, responseWithoutQuery.getStatusCode());
		assertEquals(2, responseWithoutQuery.getBody().length);

		final ResponseEntity<BuildingDto[]> responseWithNameQuery = restTemplate
				.getForEntity("/api/buildings?page=0&size=2&name=Promen", BuildingDto[].class);

		assertEquals(HttpStatus.OK, responseWithNameQuery.getStatusCode());
		assertEquals(1, responseWithNameQuery.getBody().length);
		assertEquals("Promenada", responseWithNameQuery.getBody()[0].getName());
		assertEquals(250, responseWithNameQuery.getBody()[0].getFloors());
		assertEquals(1, responseWithNameQuery.getBody()[0].getElevators().size());
	}
	
	@Test
	public void testComments() {
		
		final ResponseEntity<CommentDto[]> response = restTemplate
				.getForEntity("/api/buildings/comments", CommentDto[].class);
		
		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertEquals(2, response.getBody().length);
	}
	
	@Test
	public void testEditBuilding() {
		
		HttpHeaders headers = new HttpHeaders();
		headers.add("X-Auth-Token", token.getToken());
		Building building = buildingService.findByNameContains("omenada", null).getContent().get(0);
		BuildingDto buildingDto = new BuildingDto();
		buildingDto.setFloors(32);
		buildingDto.setName("Promenada");
		HttpEntity<Object> httpEntity = new HttpEntity<Object>(buildingDto, headers);
		
		ResponseEntity<BuildingDto> response = restTemplate.exchange("/api/buildings/"+building.getId(), 
				HttpMethod.PUT, 
				httpEntity, BuildingDto.class);
		
		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertEquals(32, response.getBody().getFloors());
		assertEquals("Promenada", response.getBody().getName());
	}
	
	@Test
	public void testDelete() {
		HttpHeaders headers = new HttpHeaders();
		headers.add("X-Auth-Token", token.getToken());
		HttpEntity<Object> httpEntity = new HttpEntity<Object>(headers);
		
		Building building = new Building(null, "Mercator", 120, null);
		buildingService.save(building);
		
		int size = buildingRepository.findAll().size();
		
		ResponseEntity<Void> response = restTemplate.exchange("/api/buildings/"+ building.getId(), 
				HttpMethod.DELETE,
				httpEntity,
				Void.class);
		
		assertEquals(size - 1, buildingRepository.findAll().size());
		assertEquals(HttpStatus.OK, response.getStatusCode());
	}
		
	@Test
	public void testAdd() {
		int size = buildingService.findAll().size();
		
		HttpHeaders headers = new HttpHeaders();
		headers.add("X-Auth-Token", token.getToken());
		
		BuildingDto buildingDto = new BuildingDto();
		buildingDto.setName("Villa Plaza");
		buildingDto.setFloors(125);
		
		HttpEntity<Object> httpEntity = new HttpEntity<Object>(buildingDto, headers);
		
		ResponseEntity<BuildingDto> response = restTemplate.exchange("/api/buildings",
				HttpMethod.POST, 
				httpEntity,
				BuildingDto.class);
		
		assertEquals(HttpStatus.CREATED, response.getStatusCode());
		assertEquals(size + 1, buildingService.findAll().size());
		assertEquals("Villa Plaza", response.getBody().getName());
		assertEquals(125, response.getBody().getFloors());
		assertEquals(0, response.getBody().getElevators().size());
		
	}
	
	@Test
	public void testAddComment() {
		int size = commentService.findAll().size();
		
		CommentDto commentDto = new CommentDto();
		commentDto.setNickname("paula");
		commentDto.setContent("blabla");
		
		HttpEntity<Object> httpEntity = new HttpEntity<Object>(commentDto);
		
		ResponseEntity<CommentDto> response = restTemplate.postForEntity("/api/buildings/comments", 
				httpEntity, CommentDto.class);
		
		assertEquals(HttpStatus.CREATED, response.getStatusCode());
		assertEquals(size + 1, commentService.findAll().size());
		assertEquals("paula", response.getBody().getNickname());
		assertEquals("blabla", response.getBody().getContent());
		assertEquals(0, response.getBody().getLikes());
		assertEquals(0, response.getBody().getDislikes());
	}
	
	@Test
	public void testLikeComment() {
		Comment comment = new Comment(null, "petar", "lorem ipsum", 0, 0);
		Comment savedComment = commentService.save(comment);
		
		int likes = commentService.findById(comment.getId()).get().getLikes();
		HttpEntity<CommentDto> httpEntity = new HttpEntity<CommentDto>(new CommentDto(savedComment));
		
		ResponseEntity<CommentDto> response = restTemplate.exchange("/api/buildings/comments/"+savedComment.getId() + "/like", 
				HttpMethod.PUT,
				httpEntity,
				CommentDto.class);
		
		assertEquals(1, response.getBody().getLikes());
	}
}

package com.liftovi.service;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.liftovi.data.ElevatorRepository;
import com.liftovi.model.Elevator;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ElevatorServiceIntegrationTest {
	@Autowired
	private ElevatorRepository elevatorRepository;
	
	@Autowired
	private ElevatorService elevatorService;
	
	Elevator testElevator;
	
	@After
	public void tearDown() {
		elevatorRepository.delete(testElevator);
	}
	
	@Test
	public void testCheckIfShouldBeWorking() {
		// lift koji ne bi trebao da radi
		testElevator = new Elevator(null, 30, 0, 0, true, null);
				
		// ovim dobijemo id koji ce repo dati testElevatoru
		testElevator = elevatorRepository.save(testElevator);
		
		elevatorService.checkIfElevatorsShouldBeWorking();
		
		// broj liftova sa 0 preostalih spratova koji rade
		long countBrokenWorkingElevators = elevatorRepository.findAll().stream()
			.filter(elevator -> elevator.getFloorsLeft() == 0 && elevator.isWorking())
			.count();
		
		assertEquals(0, countBrokenWorkingElevators);
	}
}

package com.liftovi.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.OptionalInt;
import java.util.Set;
import java.util.stream.Collectors;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit4.SpringRunner;

import com.liftovi.data.ElevatorRepository;
import com.liftovi.model.Building;
import com.liftovi.model.Elevator;
import static org.mockito.BDDMockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class ElevatorServiceUnitTest {

	@Autowired
	ElevatorService elevatorService;

	@Autowired
	BuildingService buildingService;

	@MockBean
	ElevatorRepository elevatorRepository;

	static Building building = new Building(1L, "Arkad", 25, null);
	static Elevator testElevator = new Elevator(null, 30, 0, 0, true, building);
	

	@Before
	public void setUp() {

		Set<Elevator> elevators = new HashSet<Elevator>();
		elevators.add(testElevator);
		
		building.setElevators(elevators);
		
		when(elevatorRepository.findAll()).thenReturn(elevators.stream().collect(Collectors.toList()));
		when(elevatorRepository.findAllByBuildingIdAndWorkingTrue(building.getId()))
				.thenReturn(elevators.stream().collect(Collectors.toList()));
		when(elevatorRepository.findAll()).thenReturn(elevators.stream().collect(Collectors.toList()));
		
		Page<Elevator> pagedElevators = new PageImpl<Elevator>(elevators.stream().collect(Collectors.toList()));
		Pageable pageRequest = PageRequest.of(0, 4);

		given(elevatorRepository.findAllByBuildingIdAndCode(building.getId(), testElevator.getCode(), pageRequest))
				.willReturn(pagedElevators);
		
		Page<Elevator> notWorkingElevator = new PageImpl<Elevator>(new ArrayList<Elevator>());
		
		given(elevatorRepository.findAllByBuildingIdAndWorking(building.getId(), false, pageRequest))
		.willReturn(notWorkingElevator);
				
		given(elevatorRepository.findAllByBuildingId(building.getId(), pageRequest)).willReturn(pagedElevators);

	}

	@Test
	public void testFindAllByBuildingIdAndCode() {

		Pageable pageRequest = PageRequest.of(0, 4);

		Page<Elevator> result = elevatorService.findAllByBuildingIdAndCode(building.getId(), 
				testElevator.getCode(), pageRequest);

		assertEquals(1, result.getContent().size());
		assertEquals(30, result.getContent().get(0).getCode());
		assertEquals(0, result.getContent().get(0).getCurrentFloor());
		assertEquals(0, result.getContent().get(0).getFloorsLeft());
		assertTrue(result.getContent().get(0).isWorking());
	}

	@Test
	public void testFindAllByBuildingIdAndWorking() {
		Pageable pageRequest = PageRequest.of(0, 4);
		Set<Elevator> elevators = new HashSet<Elevator>();
		Elevator testElevator = new Elevator(null, 30, 0, 0, true, building);

		elevators.add(testElevator);
		Page<Elevator> pagedElevators = new PageImpl<Elevator>(elevators.stream().collect(Collectors.toList()));

		given(elevatorRepository.findAllByBuildingIdAndWorking(building.getId(), true, pageRequest))
		.willReturn(pagedElevators);
		

		Page<Elevator> resultNotWorking = elevatorService.findAllByBuildingIdAndWorking(building.getId(), false, pageRequest);

		assertEquals(0, resultNotWorking.getContent().size());
		assertTrue(resultNotWorking.getContent().isEmpty());

		Page<Elevator> resultWorking = elevatorService.findAllByBuildingIdAndWorking(building.getId(), true,
				pageRequest);

		assertEquals(0, resultWorking.getContent().get(0).getCurrentFloor());
		assertEquals(0, resultWorking.getContent().get(0).getFloorsLeft());
		assertTrue(resultWorking.getContent().get(0).isWorking());
	}

	@Test
	public void testFindAllByBuildingId() {
		
		Pageable pageRequest = PageRequest.of(0, 4);

		Page<Elevator> result = elevatorService.findAllByBuildingId(building.getId(), pageRequest);

		assertEquals(1, result.getContent().size());
	}

	@Test
	public void testFindAllByBuildingIdAndCodeAndWorking() {
		Pageable pageRequest = PageRequest.of(0, 4);
		Set<Elevator> elevators = new HashSet<Elevator>();
		Elevator testElevator = new Elevator(null, 30, 0, 0, true, building);

		elevators.add(testElevator);
		Page<Elevator> pagedElevators = new PageImpl<Elevator>(elevators.stream().collect(Collectors.toList()));
		given(elevatorRepository.findAllByBuildingIdAndCodeAndWorking(building.getId(), 11, true, pageRequest))
		.willReturn(pagedElevators);

		Page<Elevator> result = elevatorService.findAllByBuildingIdAndCodeAndWorking(building.getId(), 11, true,
				pageRequest);
		assertEquals(1, result.getContent().size());
		assertEquals(30, result.getContent().get(0).getCode());
		assertEquals(0, result.getContent().get(0).getCurrentFloor());
		assertEquals(0, result.getContent().get(0).getFloorsLeft());
		assertTrue(result.getContent().get(0).isWorking());
	}
	
	@Test
	public void testCheckIfShouldBeWorking() {

		elevatorService.checkIfElevatorsShouldBeWorking();

		long countBrokenWorkingElevators = elevatorRepository.findAll().stream()
				.filter(elevator -> elevator.getFloorsLeft() == 0 && elevator.isWorking()).count();

		assertEquals(0, countBrokenWorkingElevators);
	}

	@Test
	public void testRideElevator() {

		OptionalInt elevatorCode = elevatorService.rideElevator(building.getId(), 6, 15);

		assertEquals(testElevator.getCode(), elevatorCode.getAsInt());
		assertEquals(15, testElevator.getCurrentFloor());
		assertEquals(0, testElevator.getFloorsLeft());
		assertFalse(testElevator.isWorking());
	}
}

package com.liftovi.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.BDDMockito.*;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

import com.liftovi.data.BuildingRepository;
import com.liftovi.model.Building;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class BuildingServiceUnitTest {

	@Autowired
	BuildingService buildingService;

	@MockBean
	BuildingRepository buildingRepository;
	
	@Before 
	public void setUp() {
		Building building = new Building(1L, "Promenada", 250, null);
		List<Building> buildings = new ArrayList<Building>();
		buildings.add(building);
		Page<Building> pagedBuildings = new PageImpl<Building>(buildings);
		
		Pageable pageRequest = PageRequest.of(0, 4);

		when(buildingRepository.findByNameContains("ome", pageRequest))
			.thenReturn(pagedBuildings);
	}

	@Test
	public void testFindByNameContains() {
		
		Pageable pageRequest = PageRequest.of(0, 4);

		List<Building> buildings = buildingService.findByNameContains("ome", pageRequest).getContent();
		
		assertEquals(1, buildings.size());
		assertEquals("Promenada", buildings.get(0).getName());
		assertEquals(250, buildings.get(0).getFloors());
		assertNull(buildings.get(0).getElevators());
	}
}

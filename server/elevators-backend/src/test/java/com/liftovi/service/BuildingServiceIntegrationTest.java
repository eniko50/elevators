package com.liftovi.service;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.liftovi.data.BuildingRepository;
import com.liftovi.model.Building;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment=SpringBootTest.WebEnvironment.RANDOM_PORT)
public class BuildingServiceIntegrationTest {
	
	@Autowired
	BuildingService buildingService;
	
	@Autowired
	BuildingRepository buildingRepository;
	
	@Test
	public void testFindAll() {
		List<Building> buildingsFromService = buildingService.findAll();
		List<Building> buildingsFromRepo = buildingRepository.findAll();
		
		assertEquals(buildingsFromService.size(), buildingsFromRepo.size());
	}
	
}

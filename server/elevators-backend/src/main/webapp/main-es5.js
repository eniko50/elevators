(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"], {
        /***/ "./$$_lazy_route_resource lazy recursive": 
        /*!******************************************************!*\
          !*** ./$$_lazy_route_resource lazy namespace object ***!
          \******************************************************/
        /*! no static exports found */
        /***/ (function (module, exports) {
            function webpackEmptyAsyncContext(req) {
                // Here Promise.resolve().then() is used instead of new Promise() to prevent
                // uncaught exception popping up in devtools
                return Promise.resolve().then(function () {
                    var e = new Error("Cannot find module '" + req + "'");
                    e.code = 'MODULE_NOT_FOUND';
                    throw e;
                });
            }
            webpackEmptyAsyncContext.keys = function () { return []; };
            webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
            module.exports = webpackEmptyAsyncContext;
            webpackEmptyAsyncContext.id = "./$$_lazy_route_resource lazy recursive";
            /***/ 
        }),
        /***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html": 
        /*!**************************************************************************!*\
          !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html ***!
          \**************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("<nav class=\"navbar navbar-expand-sm fixed-top navbar-dark shadow-lg\" style=\"background-color: darkcyan;\">\r\n  <a class=\"navbar-brand\" href=\"#\">\r\n    <img class=\"rounded-circle\" style=\"height: 40px;\" src=\"../assets/static/logo-2144403_1280.png\">\r\n  </a>\r\n  <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarText\"\r\n    aria-controls=\"navbarText\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">\r\n    <span class=\"navbar-toggler-icon\"></span>\r\n  </button>\r\n  <div class=\"collapse navbar-collapse\" id=\"navbarText\">\r\n    <ul class=\"navbar-nav mr-auto\">\r\n      <li class=\"nav-item active\">\r\n        <a class=\"nav-link\" routerLink=\"/home\">Home\r\n          <span class=\"sr-only\">(current)</span>\r\n        </a>\r\n      </li>\r\n      <li class=\"nav-item\">\r\n        <a class=\"nav-link\" routerLink=\"/about\">About</a>\r\n      </li>\r\n      <li class=\"nav-item\">\r\n        <a class=\"nav-link\" routerLink=\"/buildings\">Buldings</a>\r\n      </li>\r\n    </ul>\r\n    <ul class=\"nav navbar-nav pull-right mr-5\">\r\n      <li *ngIf=\"isLoggedIn()\" class=\"nav-item\">\r\n        <a (click)=\"logOut()\" class=\"nav-link\">Logout</a>\r\n      </li>\r\n      <li *ngIf=\"!isLoggedIn()\" class=\"nav-item d-md-flex\">\r\n        <a routerLink=\"/login\" class=\"nav-link\">Login</a>\r\n        <a routerLink=\"/register\" class=\"nav-link\">Register</a>\r\n      </li>\r\n    </ul>\r\n  </div>\r\n</nav>\r\n       \r\n<router-outlet></router-outlet>");
            /***/ 
        }),
        /***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/building-list/building-list.component.html": 
        /*!**************************************************************************************************!*\
          !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/building-list/building-list.component.html ***!
          \**************************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("<br>\n<div class=\"table-responsive-sm\">\n<table class=\"table table-hover\">\n    <thead>\n        <tr>\n            <th>#</th>\n            <th>Name</th>\n            <th>Floors</th>\n\n            <th *ngIf=\"isLoggedIn\">Ride elevator</th>\n\n            <th *ngIf=\"isAdministrator\">Action</th>\n        </tr>\n    </thead>\n    <tbody>\n        <tr *ngFor=\"let building of buildings\">\n            <td>{{building.id}}</td>\n            <td>{{building.name}}</td>\n            <td>{{building.floors}}</td>\n\n            <td><button *ngIf=\"isLoggedIn\" class=\"btn btn-outline-dark\" (click)=\"rideElevator(building.id)\">Ride\n                    elevator</button></td>\n\n            <td><button *ngIf=\"isAdministrator\" class=\"btn btn-outline-dark mr-md-2\"\n                    [routerLink]=\"[building.id, 'elevators']\">Show elevators</button>\n                <button *ngIf=\"isAdministrator\" class=\"btn btn-outline-info mr-md-2\"\n                    (click)=\"editBuilding(building)\">Edit</button>\n                <button *ngIf=\"isAdministrator\" class=\"btn btn-danger mr-md-2\"\n                    (click)=\"deleteBuilding(building.id)\">Delete</button></td>\n        </tr>\n    </tbody>\n</table>\n</div>\n<button *ngIf=\"isAdministrator\" class=\"btn btn-primary mb-5 shadow-lg\" routerLink=\"/buildings/add\">Add new building</button>\n");
            /***/ 
        }),
        /***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/comment-list/comment-list.component.html": 
        /*!************************************************************************************************!*\
          !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/comment-list/comment-list.component.html ***!
          \************************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("<div style=\"margin-top: 250px;\">\n    <form class=\"form-group my-5 py-5\" (ngSubmit)=\"postComment()\">\n        <h1>Leave a Reply</h1>\n        <input  class=\"form-control\" type=\"text\" [(ngModel)]=\"newComment.nickname\" name=\"input-nickname\" placeholder=\"Enter a nickname...\">\n        <textarea class=\"form-control\" rows=\"5\" [(ngModel)]=\"newComment.content\" name=\"input-comment\" placeholder=\"Type a comment here..\"></textarea>\n        <input class=\"btn btn-success float-right mb-5 mt-2\" type=\"submit\" value=\"Post comment\">\n    </form>\n</div>\n\n<div *ngFor=\"let comment of comments\" class=\"card\">\n    <div class=\"card-header font-weight-bold\">{{comment.nickname}}</div>\n    <div class=\"card-body\">{{comment.content}}</div>\n    <div class=\"card-footer\"><button (click)=\"likeComment(comment)\" class=\"fas fa-thumbs-up\"></button>{{comment.likes}} <button (click)=\"dislikeComment(comment)\" class=\"fas fa-thumbs-down\" style=\"color: red;\"></button>{{comment.dislikes}}</div>\n</div>");
            /***/ 
        }),
        /***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/elevator-list/elevator-list.component.html": 
        /*!**************************************************************************************************!*\
          !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/elevator-list/elevator-list.component.html ***!
          \**************************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("<br>\n<div class=\"table-responsive-sm\">\n    <table class=\"table table-hover\">\n        <thead>\n            <tr>\n                <th>#</th>\n                <th>Code</th>\n                <th>Current floor</th>\n                <th>Floors left</th>\n                <th>Working</th>\n                <th>Action</th>\n            </tr>\n        </thead>\n        <tbody>\n            <tr *ngFor=\"let elevator of elevators\">\n                <td>{{elevator.id}}</td>\n                <td>{{elevator.code}}</td>\n                <td>{{elevator.currentFloor}}</td>\n                <td>{{elevator.floorsLeft}}</td>\n                <td>{{elevator.working}}</td>\n                <td><button class=\"btn btn-outline-info mr-md-2\" (click)=\"editElevator(elevator)\">Edit</button>\n                <button class=\"btn btn-danger mr-md-2\" (click)=\"deleteElevator(elevator.id)\">Delete</button></td>\n            </tr>\n        </tbody>\n    </table>\n</div>\n<button *ngIf=\"isAdministrator\" class=\"btn btn-primary mb-5 shadow-lg\" routerLink=\"./add\">Add new elevator</button>\n\n");
            /***/ 
        }),
        /***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/forms/add-building/add-building.component.html": 
        /*!******************************************************************************************************!*\
          !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/forms/add-building/add-building.component.html ***!
          \******************************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("<div style=\"margin-bottom: 250px;\">\n    <form class=\"form-group d-md-table container col-md-3 col-6\" (ngSubmit)=\"addBuilding(buildingToAdd)\">\n        <label for=\"building-name\">Name </label>\n        <input class=\"form-control\" name=\"building-name\" type=\"text\" [(ngModel)]=\"buildingToAdd.name\"\n            placeholder=\"Name\">\n        <label for=\"building-floors\">Floors </label>\n        <input class=\"form-control\" name=\"building-floors\" type=\"number\" [(ngModel)]=\"buildingToAdd.floors\"\n            placeholder=\"Floors\">\n        <input class=\"form-control btn btn-primary mt-2 mb-5\" type=\"submit\" value=\"Add\">\n    </form>\n</div>");
            /***/ 
        }),
        /***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/forms/add-elevator/add-elevator.component.html": 
        /*!******************************************************************************************************!*\
          !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/forms/add-elevator/add-elevator.component.html ***!
          \******************************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("<div style=\"margin-bottom: 200px;\">\n    <form class=\"form-group d-md-table container col-md-3 col-6\" (ngSubmit)=\"addElevator(elevatorToAdd)\">\n        <label for=\"elevator-code\">Code </label>\n        <input class=\"form-control\" name=\"elevator-code\" type=\"number\" [(ngModel)]=\"elevatorToAdd.code\"\n            placeholder=\"Code\">\n        <label for=\"elevator-current-floor\">Current floor</label>\n        <input class=\"form-control\" name=\"elevator-current-floor\" type=\"number\" [(ngModel)]=\"elevatorToAdd.currentFloor\"\n            placeholder=\"Current floor\">\n        <label for=\"elevator-floors-left\">Floors left</label>\n        <input class=\"form-control\" name=\"elevator-floors-left\" type=\"number\" [(ngModel)]=\"elevatorToAdd.floorsLeft\"\n            placeholder=\"Floors left\">\n        <label for=\"elevator-working\">Working</label>\n        <input class=\"ml-3\" name=\"elevator-working\" type=\"checkbox\" [(ngModel)]=\"elevatorToAdd.working\">\n        <input class=\"form-control btn btn-primary mt-2 mb-5\" type=\"submit\" value=\"Add\">\n    </form>\n</div>");
            /***/ 
        }),
        /***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/forms/edit-building/edit-building.component.html": 
        /*!********************************************************************************************************!*\
          !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/forms/edit-building/edit-building.component.html ***!
          \********************************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("<h4>Choose building to edit...</h4>\n<br>\n<form class=\"form-group\" (ngSubmit)=\"editBuilding()\">\n    <label for=\"building-id\">Id </label>\n    <input class=\"form-control\" name=\"building-id\" type=\"text\" [(ngModel)]=\"buildingToEdit.id\" placeholder=\"Id\"\n        disabled>\n    <label for=\"building-name\">Name </label>\n    <input class=\"form-control\" name=\"building-name\" type=\"text\" [(ngModel)]=\"buildingToEdit.name\" placeholder=\"Name\">\n    <label for=\"building-floors\">Floors </label>\n    <input class=\"form-control\" name=\"building-floors\" type=\"text\" [(ngModel)]=\"buildingToEdit.floors\"\n        placeholder=\"Floors\">\n    <input class=\"btn btn-info form-control mt-2 shadow-lg\" type=\"submit\" value=\"Edit\">\n</form>");
            /***/ 
        }),
        /***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/forms/edit-elevator/edit-elevator.component.html": 
        /*!********************************************************************************************************!*\
          !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/forms/edit-elevator/edit-elevator.component.html ***!
          \********************************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("<div class=\"d-table justifly-content-center mx-auto\">\n        <h4>Choose elevator to edit...</h4>\n        <br>\n        <form class=\"form-group\" (ngSubmit)=\"editElevator()\">\n            <label for=\"elevator-id\">Id</label>\n            <input class=\"form-control col-md-12\" name=\"elevator-id\" type=\"text\" [(ngModel)]=\"elevatorToEdit.id\"\n                disabled placeholder=\"Id\">\n            <label for=\"elevator-code\">Code </label>\n            <input class=\"form-control col-md-12\" name=\"elevator-code\" type=\"number\"\n                [(ngModel)]=\"elevatorToEdit.code\" placeholder=\"Code\">\n            <label for=\"elevator-current-floor\">Current floor </label>\n            <input class=\"form-control col-md-12\" name=\"elevator-current-floor\" type=\"number\"\n                [(ngModel)]=\"elevatorToEdit.currentFloor\" placeholder=\"Current floor\">\n            <label for=\"elevator-floors-left\">Floors left </label>\n            <input class=\"form-control col-md-12\" name=\"elevator-floors-left\" type=\"number\"\n                [(ngModel)]=\"elevatorToEdit.floorsLeft\" placeholder=\"Floors Left\">\n            <input class=\"btn btn-info form-control col-md-12 mt-2 shadow-lg\" type=\"submit\" value=\"Edit\">\n        </form>\n</div>");
            /***/ 
        }),
        /***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/forms/ride-elevator/ride-elevator.component.html": 
        /*!********************************************************************************************************!*\
          !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/forms/ride-elevator/ride-elevator.component.html ***!
          \********************************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("<form class=\"form-inline pt-3\" (ngSubmit)=\"rideElevator()\">\n    <label for=\"building-id\">Building id </label>\n    <input class=\"form-control mr-md-3 mr-2 ml-2 col-md-1 col-2\" name=\"building-id\" type=\"number\" [(ngModel)]=\"buildingId\" placeholder=\"id\">\n    <label for=\"start-floor\">Start </label>\n    <input class=\"form-control mr-md-3 mr-2 ml-2 col-md-1 col-2\" name=\"start-floor\" type=\"number\" [(ngModel)]=\"elevatorToRide.startFloor\" placeholder=\"Start floor\">\n    <label for=\"end-floor\">End </label>\n    <input class=\"form-control ml-2 mr-md-2 col-md-1 col-2\" name=\"end-floor\" type=\"number\" [(ngModel)]=\"elevatorToRide.endFloor\" placeholder=\"End floor\">\n    <input class=\"form-control btn custom-btn col-md-2 mt-2 mt-sm-0 shadow-lg\" type=\"submit\" value=\"Ride\">\n</form>");
            /***/ 
        }),
        /***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/login/login.component.html": 
        /*!**********************************************************************************!*\
          !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/login/login.component.html ***!
          \**********************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("<div class=\"col-md-3 col-6 container my-5 pt-5\" style=\"padding-bottom: 200px;\">\n    <h3>Sign in</h3>\n\n    <form class=\"form-group\" (ngSubmit)=\"login()\">\n        <label for=\"username\">Username </label>\n        <input class=\"form-control\" type=\"text\" id=\"username\" name=\"username\" [(ngModel)]=\"user.username\"\n            placeholder=\"Username\" required autofocus>\n        <label for=\"inputPassword\">Password </label>\n        <input class=\"form-control\" type=\"password\" id=\"inputPassword\" name=\"username\" [(ngModel)]=\"user.password\"\n            placeholder=\"Password\" required>\n        <button class=\"btn btn-primary btn-block mt-2\" type=\"submit\">Sign in</button>\n    </form>\n    <br>\n    <div *ngIf=wrongUsernameOrPass role=\"alert\">\n        Wrong username or password.\n    </div>\n</div>");
            /***/ 
        }),
        /***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/about-page/about-page.component.html": 
        /*!**************************************************************************************************!*\
          !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/about-page/about-page.component.html ***!
          \**************************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("<div class=\"m-5 p-5 text-center\">\n    <div style=\"padding-bottom: 400px;\">\n        <h1>About Us</h1>\n        <p>\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et\n            dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex\n            ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat\n            nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit\n            anim id est laborum.\"</p>\n    </div>\n</div>");
            /***/ 
        }),
        /***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/building-page/building-page.component.html": 
        /*!********************************************************************************************************!*\
          !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/building-page/building-page.component.html ***!
          \********************************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("<div class=\"container-fluid pt-5 mt-5\" style=\"padding-bottom: 300px;\">\n    <div class=\"row\">\n        <div class=\"float-left ml-3 col-md-8\">\n            <app-search (setSearchTerm)=\"search($event)\"></app-search>\n            <ul class=\"pagination float-right\">\n              <li class=\"page-item\"> <a class=\"page-link\" disabled=\"page <=0\" (click)=\"prevPage()\">Prev</a></li> \n              <li class=\"page-item\"> <a class=\"page-link\" disabled=\"page <=0\" (click)=\"callPage(0)\">1</a></li>\n              <li class=\"page-item\"> <a class=\"page-link\" disabled=\"page <=0\" (click)=\"callPage(1)\">2</a></li>\n              <li class=\"page-item\"> <a class=\"page-link\" disabled=\"page <=0\" (click)=\"callPage(2)\">3</a></li>\n              <li class=\"page-item\"><a class=\"page-link\" (click)=\"nextPage()\">Next</a></li>\n            </ul>\n            <app-building-list [isAdministrator]=\"isAdministrator\" [buildings]=\"buildings\"\n                (markBuildingForEditing)=\"setActiveBuilding($event)\" (buildingToDelete)=\"deleteBuilding($event)\"\n                [isLoggedIn]=\"isLoggedIn\" (markBuildingIdForElevatorForRiding)=\"setBuildingIdForElevatorRiding($event)\">\n            </app-building-list>\n        </div>\n        <app-edit-building class=\"mr-md-3 pt-md-3 col-md-3 col-6 container-sm\" *ngIf=\"isAdministrator\"\n            (buildingEdited)=\"editBuilding($event)\" [buildingToEdit]=\"activeBuilding\"></app-edit-building>\n    </div>\n    <app-ride-elevator *ngIf=\"isLoggedIn\" [buildingId]=\"activeBuildingId\" (elevatorRided)=\"rideElevator($event)\">\n    </app-ride-elevator>\n    <app-comment-list [comments]=\"comments\" (commentLiked)=\"likeComment($event)\" (commentDisliked)=\"dislikeComment($event)\"\n    (commentPosted)=\"postComment($event)\"></app-comment-list>\n</div>");
            /***/ 
        }),
        /***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/elevator-page/elevator-page.component.html": 
        /*!********************************************************************************************************!*\
          !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/elevator-page/elevator-page.component.html ***!
          \********************************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("<div class=\"container-fluid pt-5 mt-5\" style=\"padding-bottom: 300px;\">\r\n    <div class=\"row justify-content-between\">\r\n        <div class=\"float-left ml-3 col-md-8\">\r\n            <app-filter (setWorking)=\"setWorking($event)\" (setCode)=\"setCode($event)\"></app-filter>\r\n            <ul class=\"pagination float-right\">\r\n              <li class=\"page-item\"> <a class=\"page-link\" disabled=\"page <=0\" (click)=\"prevPage()\">Prev</a></li> \r\n              <li class=\"page-item\"> <a class=\"page-link\" disabled=\"page <=0\" (click)=\"callPage(0)\">1</a></li>\r\n              <li class=\"page-item\"> <a class=\"page-link\" disabled=\"page <=0\" (click)=\"callPage(1)\">2</a></li>\r\n              <li class=\"page-item\"> <a class=\"page-link\" disabled=\"page <=0\" (click)=\"callPage(2)\">3</a></li>\r\n              <li class=\"page-item\"><a class=\"page-link\" (click)=\"nextPage()\">Next</a></li>\r\n            </ul>\r\n        </div>\r\n        <app-elevator-list class=\"col-md-9\" [isAdministrator]=\"isAdministrator\" *ngIf=\"isAdministrator\"\r\n            [elevators]=\"elevators\" (elevatorToDelete)=\"deleteElevator($event)\"\r\n            (markElevatorForEditing)=\"setActiveElevator($event)\">\r\n        </app-elevator-list>\r\n        <app-edit-elevator class=\"pt-md-3 pr-md-3 col-md-3 col-6 container-sm\" [isAdministrator]=\"isAdministrator\" *ngIf=\"isAdministrator\" class=\"pt-md-3 pr-md-3 col-md-3\"\r\n            [elevatorToEdit]=\"activeElevator\" (elevatorEdited)=\"editElevator($event)\"></app-edit-elevator>\r\n    </div>\r\n</div>");
            /***/ 
        }),
        /***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/home-page/home-page.component.html": 
        /*!************************************************************************************************!*\
          !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/home-page/home-page.component.html ***!
          \************************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("<div class=\"row mx-auto\">\n    <div class=\"col-12 text-center p-0 d-flex align-items-center justify-content-center\">\n        <img class=\"custom-img\" src=\"../../../assets/static/night-street-city-high-quality-1080P-wallpaper.jpg\" alt=\"Sity\">\n        <div class=\"position-absolute text-white\">\n            <h1 class=\"text-uppercase font-weight-bold my-auto mx-auto\">Rent rooms, see informations<br> in most hotels in the county...\n            </h1>\n            <button class=\"btn custom-btn\" routerLink=\"/about\">More info</button>\n        </div>\n    </div>\n</div>\n\n<div class=\"row my-3 mx-3 text-center align\">\n    <div class=\"card col-md-4 col-sm-6 bg-light shadow align-items-center\">\n        <img class=\"mt-2 card-img-top img-thumbnail rounded-circle\" src=\"../../../assets/static/logo-2144403_1280.png\">\n        <div class=\"card-body\">\n            <h5 class=\"card-title\">Why us?</h5>\n            <p class=\"card-text text-info\">Communication, efficiency, convenience...</p>\n        </div>\n    </div>\n\n    <div class=\"card col-md-4 col-sm-6 bg-light shadow align-items-center\">\n        <img class=\"mt-2 card-img-top img-thumbnail rounded-circle\" src=\"../../../assets/static/Free-City.jpg\">\n        <div class=\"card-body\">\n            <h5 class=\"card-title\">News</h5>\n            <a class=\"card-link\" href=\"#\">See news...</a>\n        </div>\n    </div>\n\n    <div class=\"card col-md-4 col-sm-6 mx-auto bg-light shadow align-items-center\">\n        <img class=\"mt-2 card-img-top img-thumbnail rounded-circle\"\n            src=\"../../../assets/static/Map-Locator.jpg\" alt=\"Card image\">\n        <div class=\"card-body\">\n            <h5 class=\"card-title\">Maps</h5>\n            <a class=\"card-link\" href=\"#\">See maps...</a>\n        </div>\n    </div>\n</div>");
            /***/ 
        }),
        /***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/page-not-found/page-not-found.component.html": 
        /*!**********************************************************************************************************!*\
          !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/page-not-found/page-not-found.component.html ***!
          \**********************************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("<div class=\"d-table mx-auto my-5 pt-5\" style=\"padding-bottom: 300px;\">\n    <h1 class=\"py-5\">Page not found!</h1>\n</div>\n");
            /***/ 
        }),
        /***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/register/register.component.html": 
        /*!****************************************************************************************!*\
          !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/register/register.component.html ***!
          \****************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("<div class=\"col-md-3 col-6 container my-5 py-5\">\n    <h3>Register</h3>\n\n    <form class=\"form-group\" (ngSubmit)=\"register()\">\n        <label for=\"firstName\">First name </label>\n        <input class=\"form-control\" type=\"text\" id=\"firstName\" name=\"firstName\" [(ngModel)]=\"user.firstName\"\n            placeholder=\"First name\" required autofocus>\n        <label for=\"lastName\">Last name </label>\n        <input class=\"form-control\" type=\"text\" id=\"lastName\" name=\"lastName\" [(ngModel)]=\"user.lastName\"\n            placeholder=\"Last name\" required>\n        <label for=\"username\">Username </label>\n        <input class=\"form-control\" type=\"text\" id=\"username\" name=\"username\" [(ngModel)]=\"user.username\"\n            placeholder=\"Username\" required>\n        <label for=\"inputPassword\">Password </label>\n        <input class=\"form-control\" type=\"password\" id=\"inputPassword\" name=\"inputPassword\" [(ngModel)]=\"user.password\"\n            placeholder=\"Password\" required>\n        <button class=\"btn btn-primary btn-block mt-2\" type=\"submit\">Register</button>\n    </form>\n    <br>\n    <div *ngIf=\"usernameExists\" role=\"alert\">\n        This username alredy exits, try another.\n    </div>\n</div>");
            /***/ 
        }),
        /***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/search/building/search.component.html": 
        /*!*********************************************************************************************!*\
          !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/search/building/search.component.html ***!
          \*********************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("<form class=\"form-inline mb-5\" (ngSubmit)=\"search()\">\n    <input class=\"form-control col-sm-4 col-6\" type=\"text\" id=\"search-by-name\" name=\"search-by-name\" [(ngModel)]=\"searchTerm\" placeholder=\"Building name\">\n    <input class=\"form-control col-sm-2 col-3 btn btn-outline-dark\" type=\"submit\" value=\"Search\">\n    <button class=\"form-control col-sm-2 col-3 btn-outline-primary\" type=\"button\" (click)=\"reset()\">Reset</button>\n</form>\n");
            /***/ 
        }),
        /***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/search/elevator/filter/filter.component.html": 
        /*!****************************************************************************************************!*\
          !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/search/elevator/filter/filter.component.html ***!
          \****************************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("<form class=\"form-inline mb-5\" (ngSubmit)=\"filter()\">\n    <input class=\"form-control col-sm-4 col-6\" type=\"number\" id=\"search-by-code\" name=\"search-by-code\" [(ngModel)]=\"code\" placeholder=\"Elevator code\">\n    <label class=\"ml-2 mr-2\" for=\"search-by-working\">Working: </label>\n    <input class=\"form-control mr-2\" type=\"checkbox\" name=\"search-by-working\" [(ngModel)]=\"working\" placeholder=\"Working\">\n    <input class=\"form-control col-sm-2 col-3 btn btn-outline-dark\" type=\"submit\" value=\"Search\">\n    <button class=\"form-control col-sm-2 col-3 btn-outline-primary\" type=\"button\" (click)=\"reset()\">Reset</button>\n</form>\n");
            /***/ 
        }),
        /***/ "./node_modules/tslib/tslib.es6.js": 
        /*!*****************************************!*\
          !*** ./node_modules/tslib/tslib.es6.js ***!
          \*****************************************/
        /*! exports provided: __extends, __assign, __rest, __decorate, __param, __metadata, __awaiter, __generator, __exportStar, __values, __read, __spread, __spreadArrays, __await, __asyncGenerator, __asyncDelegator, __asyncValues, __makeTemplateObject, __importStar, __importDefault */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__extends", function () { return __extends; });
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__assign", function () { return __assign; });
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__rest", function () { return __rest; });
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__decorate", function () { return __decorate; });
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__param", function () { return __param; });
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__metadata", function () { return __metadata; });
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__awaiter", function () { return __awaiter; });
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__generator", function () { return __generator; });
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__exportStar", function () { return __exportStar; });
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__values", function () { return __values; });
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__read", function () { return __read; });
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__spread", function () { return __spread; });
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__spreadArrays", function () { return __spreadArrays; });
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__await", function () { return __await; });
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__asyncGenerator", function () { return __asyncGenerator; });
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__asyncDelegator", function () { return __asyncDelegator; });
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__asyncValues", function () { return __asyncValues; });
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__makeTemplateObject", function () { return __makeTemplateObject; });
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__importStar", function () { return __importStar; });
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__importDefault", function () { return __importDefault; });
            /*! *****************************************************************************
            Copyright (c) Microsoft Corporation. All rights reserved.
            Licensed under the Apache License, Version 2.0 (the "License"); you may not use
            this file except in compliance with the License. You may obtain a copy of the
            License at http://www.apache.org/licenses/LICENSE-2.0
            
            THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
            KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
            WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
            MERCHANTABLITY OR NON-INFRINGEMENT.
            
            See the Apache Version 2.0 License for specific language governing permissions
            and limitations under the License.
            ***************************************************************************** */
            /* global Reflect, Promise */
            var extendStatics = function (d, b) {
                extendStatics = Object.setPrototypeOf ||
                    ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
                    function (d, b) { for (var p in b)
                        if (b.hasOwnProperty(p))
                            d[p] = b[p]; };
                return extendStatics(d, b);
            };
            function __extends(d, b) {
                extendStatics(d, b);
                function __() { this.constructor = d; }
                d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
            }
            var __assign = function () {
                __assign = Object.assign || function __assign(t) {
                    for (var s, i = 1, n = arguments.length; i < n; i++) {
                        s = arguments[i];
                        for (var p in s)
                            if (Object.prototype.hasOwnProperty.call(s, p))
                                t[p] = s[p];
                    }
                    return t;
                };
                return __assign.apply(this, arguments);
            };
            function __rest(s, e) {
                var t = {};
                for (var p in s)
                    if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
                        t[p] = s[p];
                if (s != null && typeof Object.getOwnPropertySymbols === "function")
                    for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
                        if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                            t[p[i]] = s[p[i]];
                    }
                return t;
            }
            function __decorate(decorators, target, key, desc) {
                var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
                if (typeof Reflect === "object" && typeof Reflect.decorate === "function")
                    r = Reflect.decorate(decorators, target, key, desc);
                else
                    for (var i = decorators.length - 1; i >= 0; i--)
                        if (d = decorators[i])
                            r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
                return c > 3 && r && Object.defineProperty(target, key, r), r;
            }
            function __param(paramIndex, decorator) {
                return function (target, key) { decorator(target, key, paramIndex); };
            }
            function __metadata(metadataKey, metadataValue) {
                if (typeof Reflect === "object" && typeof Reflect.metadata === "function")
                    return Reflect.metadata(metadataKey, metadataValue);
            }
            function __awaiter(thisArg, _arguments, P, generator) {
                return new (P || (P = Promise))(function (resolve, reject) {
                    function fulfilled(value) { try {
                        step(generator.next(value));
                    }
                    catch (e) {
                        reject(e);
                    } }
                    function rejected(value) { try {
                        step(generator["throw"](value));
                    }
                    catch (e) {
                        reject(e);
                    } }
                    function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
                    step((generator = generator.apply(thisArg, _arguments || [])).next());
                });
            }
            function __generator(thisArg, body) {
                var _ = { label: 0, sent: function () { if (t[0] & 1)
                        throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
                return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function () { return this; }), g;
                function verb(n) { return function (v) { return step([n, v]); }; }
                function step(op) {
                    if (f)
                        throw new TypeError("Generator is already executing.");
                    while (_)
                        try {
                            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done)
                                return t;
                            if (y = 0, t)
                                op = [op[0] & 2, t.value];
                            switch (op[0]) {
                                case 0:
                                case 1:
                                    t = op;
                                    break;
                                case 4:
                                    _.label++;
                                    return { value: op[1], done: false };
                                case 5:
                                    _.label++;
                                    y = op[1];
                                    op = [0];
                                    continue;
                                case 7:
                                    op = _.ops.pop();
                                    _.trys.pop();
                                    continue;
                                default:
                                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) {
                                        _ = 0;
                                        continue;
                                    }
                                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) {
                                        _.label = op[1];
                                        break;
                                    }
                                    if (op[0] === 6 && _.label < t[1]) {
                                        _.label = t[1];
                                        t = op;
                                        break;
                                    }
                                    if (t && _.label < t[2]) {
                                        _.label = t[2];
                                        _.ops.push(op);
                                        break;
                                    }
                                    if (t[2])
                                        _.ops.pop();
                                    _.trys.pop();
                                    continue;
                            }
                            op = body.call(thisArg, _);
                        }
                        catch (e) {
                            op = [6, e];
                            y = 0;
                        }
                        finally {
                            f = t = 0;
                        }
                    if (op[0] & 5)
                        throw op[1];
                    return { value: op[0] ? op[1] : void 0, done: true };
                }
            }
            function __exportStar(m, exports) {
                for (var p in m)
                    if (!exports.hasOwnProperty(p))
                        exports[p] = m[p];
            }
            function __values(o) {
                var m = typeof Symbol === "function" && o[Symbol.iterator], i = 0;
                if (m)
                    return m.call(o);
                return {
                    next: function () {
                        if (o && i >= o.length)
                            o = void 0;
                        return { value: o && o[i++], done: !o };
                    }
                };
            }
            function __read(o, n) {
                var m = typeof Symbol === "function" && o[Symbol.iterator];
                if (!m)
                    return o;
                var i = m.call(o), r, ar = [], e;
                try {
                    while ((n === void 0 || n-- > 0) && !(r = i.next()).done)
                        ar.push(r.value);
                }
                catch (error) {
                    e = { error: error };
                }
                finally {
                    try {
                        if (r && !r.done && (m = i["return"]))
                            m.call(i);
                    }
                    finally {
                        if (e)
                            throw e.error;
                    }
                }
                return ar;
            }
            function __spread() {
                for (var ar = [], i = 0; i < arguments.length; i++)
                    ar = ar.concat(__read(arguments[i]));
                return ar;
            }
            function __spreadArrays() {
                for (var s = 0, i = 0, il = arguments.length; i < il; i++)
                    s += arguments[i].length;
                for (var r = Array(s), k = 0, i = 0; i < il; i++)
                    for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
                        r[k] = a[j];
                return r;
            }
            ;
            function __await(v) {
                return this instanceof __await ? (this.v = v, this) : new __await(v);
            }
            function __asyncGenerator(thisArg, _arguments, generator) {
                if (!Symbol.asyncIterator)
                    throw new TypeError("Symbol.asyncIterator is not defined.");
                var g = generator.apply(thisArg, _arguments || []), i, q = [];
                return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i;
                function verb(n) { if (g[n])
                    i[n] = function (v) { return new Promise(function (a, b) { q.push([n, v, a, b]) > 1 || resume(n, v); }); }; }
                function resume(n, v) { try {
                    step(g[n](v));
                }
                catch (e) {
                    settle(q[0][3], e);
                } }
                function step(r) { r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r); }
                function fulfill(value) { resume("next", value); }
                function reject(value) { resume("throw", value); }
                function settle(f, v) { if (f(v), q.shift(), q.length)
                    resume(q[0][0], q[0][1]); }
            }
            function __asyncDelegator(o) {
                var i, p;
                return i = {}, verb("next"), verb("throw", function (e) { throw e; }), verb("return"), i[Symbol.iterator] = function () { return this; }, i;
                function verb(n, f) { i[n] = o[n] ? function (v) { return (p = !p) ? { value: __await(o[n](v)), done: n === "return" } : f ? f(v) : v; } : f; }
            }
            function __asyncValues(o) {
                if (!Symbol.asyncIterator)
                    throw new TypeError("Symbol.asyncIterator is not defined.");
                var m = o[Symbol.asyncIterator], i;
                return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i);
                function verb(n) { i[n] = o[n] && function (v) { return new Promise(function (resolve, reject) { v = o[n](v), settle(resolve, reject, v.done, v.value); }); }; }
                function settle(resolve, reject, d, v) { Promise.resolve(v).then(function (v) { resolve({ value: v, done: d }); }, reject); }
            }
            function __makeTemplateObject(cooked, raw) {
                if (Object.defineProperty) {
                    Object.defineProperty(cooked, "raw", { value: raw });
                }
                else {
                    cooked.raw = raw;
                }
                return cooked;
            }
            ;
            function __importStar(mod) {
                if (mod && mod.__esModule)
                    return mod;
                var result = {};
                if (mod != null)
                    for (var k in mod)
                        if (Object.hasOwnProperty.call(mod, k))
                            result[k] = mod[k];
                result.default = mod;
                return result;
            }
            function __importDefault(mod) {
                return (mod && mod.__esModule) ? mod : { default: mod };
            }
            /***/ 
        }),
        /***/ "./src/app/app-routing.module.ts": 
        /*!***************************************!*\
          !*** ./src/app/app-routing.module.ts ***!
          \***************************************/
        /*! exports provided: AppRoutingModule */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function () { return AppRoutingModule; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
            /* harmony import */ var _pages_building_page_building_page_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./pages/building-page/building-page.component */ "./src/app/pages/building-page/building-page.component.ts");
            /* harmony import */ var _pages_page_not_found_page_not_found_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./pages/page-not-found/page-not-found.component */ "./src/app/pages/page-not-found/page-not-found.component.ts");
            /* harmony import */ var _pages_elevator_page_elevator_page_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./pages/elevator-page/elevator-page.component */ "./src/app/pages/elevator-page/elevator-page.component.ts");
            /* harmony import */ var _service_security_can_activate_auth_guard_guard__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./service/security/can-activate-auth-guard.guard */ "./src/app/service/security/can-activate-auth-guard.guard.ts");
            /* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./login/login.component */ "./src/app/login/login.component.ts");
            /* harmony import */ var _pages_home_page_home_page_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./pages/home-page/home-page.component */ "./src/app/pages/home-page/home-page.component.ts");
            /* harmony import */ var _pages_about_page_about_page_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./pages/about-page/about-page.component */ "./src/app/pages/about-page/about-page.component.ts");
            /* harmony import */ var _forms_add_building_add_building_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./forms/add-building/add-building.component */ "./src/app/forms/add-building/add-building.component.ts");
            /* harmony import */ var _forms_add_elevator_add_elevator_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./forms/add-elevator/add-elevator.component */ "./src/app/forms/add-elevator/add-elevator.component.ts");
            /* harmony import */ var _register_register_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./register/register.component */ "./src/app/register/register.component.ts");
            var routes = [
                { path: 'home', component: _pages_home_page_home_page_component__WEBPACK_IMPORTED_MODULE_8__["HomePageComponent"] },
                { path: 'about', component: _pages_about_page_about_page_component__WEBPACK_IMPORTED_MODULE_9__["AboutPageComponent"] },
                { path: 'buildings', component: _pages_building_page_building_page_component__WEBPACK_IMPORTED_MODULE_3__["BuildingPageComponent"] },
                { path: 'buildings/add', component: _forms_add_building_add_building_component__WEBPACK_IMPORTED_MODULE_10__["AddBuildingComponent"], canActivate: [_service_security_can_activate_auth_guard_guard__WEBPACK_IMPORTED_MODULE_6__["CanActivateAuthGuardGuard"]] },
                { path: 'buildings/:id/elevators/add', component: _forms_add_elevator_add_elevator_component__WEBPACK_IMPORTED_MODULE_11__["AddElevatorComponent"], canActivate: [_service_security_can_activate_auth_guard_guard__WEBPACK_IMPORTED_MODULE_6__["CanActivateAuthGuardGuard"]] },
                { path: 'buildings/:id/elevators', component: _pages_elevator_page_elevator_page_component__WEBPACK_IMPORTED_MODULE_5__["ElevatorPageComponent"], canActivate: [_service_security_can_activate_auth_guard_guard__WEBPACK_IMPORTED_MODULE_6__["CanActivateAuthGuardGuard"]] },
                { path: 'register', component: _register_register_component__WEBPACK_IMPORTED_MODULE_12__["RegisterComponent"] },
                { path: 'login', component: _login_login_component__WEBPACK_IMPORTED_MODULE_7__["LoginComponent"] },
                { path: '', redirectTo: 'home', pathMatch: 'full' },
                { path: '**', component: _pages_page_not_found_page_not_found_component__WEBPACK_IMPORTED_MODULE_4__["PageNotFoundComponent"] }
            ];
            var AppRoutingModule = /** @class */ (function () {
                function AppRoutingModule() {
                }
                return AppRoutingModule;
            }());
            AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
                    imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes)],
                    exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
                })
            ], AppRoutingModule);
            /***/ 
        }),
        /***/ "./src/app/app.component.css": 
        /*!***********************************!*\
          !*** ./src/app/app.component.css ***!
          \***********************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuY3NzIn0= */");
            /***/ 
        }),
        /***/ "./src/app/app.component.ts": 
        /*!**********************************!*\
          !*** ./src/app/app.component.ts ***!
          \**********************************/
        /*! exports provided: AppComponent */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function () { return AppComponent; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _service_security_authentication_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./service/security/authentication.service */ "./src/app/service/security/authentication.service.ts");
            /* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
            /* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
            var AppComponent = /** @class */ (function () {
                function AppComponent(authenticaionService, router, location) {
                    this.authenticaionService = authenticaionService;
                    this.router = router;
                    this.location = location;
                    this.title = 'liftovi';
                }
                AppComponent.prototype.isLoggedIn = function () {
                    return this.authenticaionService.isLoggedIn();
                };
                AppComponent.prototype.logOut = function () {
                    this.authenticaionService.logOut();
                    this.router.navigate(['/login']);
                };
                return AppComponent;
            }());
            AppComponent.ctorParameters = function () { return [
                { type: _service_security_authentication_service__WEBPACK_IMPORTED_MODULE_2__["AuthenticationService"] },
                { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
                { type: _angular_common__WEBPACK_IMPORTED_MODULE_4__["Location"] }
            ]; };
            AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
                    selector: 'app-root',
                    template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./app.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html")).default,
                    styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")).default]
                })
            ], AppComponent);
            /***/ 
        }),
        /***/ "./src/app/app.module.ts": 
        /*!*******************************!*\
          !*** ./src/app/app.module.ts ***!
          \*******************************/
        /*! exports provided: AppModule */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function () { return AppModule; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
            /* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
            /* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
            /* harmony import */ var _building_list_building_list_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./building-list/building-list.component */ "./src/app/building-list/building-list.component.ts");
            /* harmony import */ var _pages_building_page_building_page_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./pages/building-page/building-page.component */ "./src/app/pages/building-page/building-page.component.ts");
            /* harmony import */ var _pages_page_not_found_page_not_found_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./pages/page-not-found/page-not-found.component */ "./src/app/pages/page-not-found/page-not-found.component.ts");
            /* harmony import */ var _forms_edit_building_edit_building_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./forms/edit-building/edit-building.component */ "./src/app/forms/edit-building/edit-building.component.ts");
            /* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
            /* harmony import */ var _forms_add_building_add_building_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./forms/add-building/add-building.component */ "./src/app/forms/add-building/add-building.component.ts");
            /* harmony import */ var _pages_elevator_page_elevator_page_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./pages/elevator-page/elevator-page.component */ "./src/app/pages/elevator-page/elevator-page.component.ts");
            /* harmony import */ var _elevator_list_elevator_list_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./elevator-list/elevator-list.component */ "./src/app/elevator-list/elevator-list.component.ts");
            /* harmony import */ var _forms_add_elevator_add_elevator_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./forms/add-elevator/add-elevator.component */ "./src/app/forms/add-elevator/add-elevator.component.ts");
            /* harmony import */ var _forms_edit_elevator_edit_elevator_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./forms/edit-elevator/edit-elevator.component */ "./src/app/forms/edit-elevator/edit-elevator.component.ts");
            /* harmony import */ var _forms_ride_elevator_ride_elevator_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./forms/ride-elevator/ride-elevator.component */ "./src/app/forms/ride-elevator/ride-elevator.component.ts");
            /* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./login/login.component */ "./src/app/login/login.component.ts");
            /* harmony import */ var _service_security_token_interceptor_service__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./service/security/token-interceptor.service */ "./src/app/service/security/token-interceptor.service.ts");
            /* harmony import */ var _service_security_authentication_service__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./service/security/authentication.service */ "./src/app/service/security/authentication.service.ts");
            /* harmony import */ var _service_security_can_activate_auth_guard_guard__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./service/security/can-activate-auth-guard.guard */ "./src/app/service/security/can-activate-auth-guard.guard.ts");
            /* harmony import */ var _service_security_jwt_utils_service__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./service/security/jwt-utils.service */ "./src/app/service/security/jwt-utils.service.ts");
            /* harmony import */ var _pages_home_page_home_page_component__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./pages/home-page/home-page.component */ "./src/app/pages/home-page/home-page.component.ts");
            /* harmony import */ var _pages_about_page_about_page_component__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./pages/about-page/about-page.component */ "./src/app/pages/about-page/about-page.component.ts");
            /* harmony import */ var _register_register_component__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ./register/register.component */ "./src/app/register/register.component.ts");
            /* harmony import */ var _search_building_search_component__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ./search/building/search.component */ "./src/app/search/building/search.component.ts");
            /* harmony import */ var _search_elevator_filter_filter_component__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! ./search/elevator/filter/filter.component */ "./src/app/search/elevator/filter/filter.component.ts");
            /* harmony import */ var _comment_list_comment_list_component__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! ./comment-list/comment-list.component */ "./src/app/comment-list/comment-list.component.ts");
            var AppModule = /** @class */ (function () {
                function AppModule() {
                }
                return AppModule;
            }());
            AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
                    declarations: [
                        _app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"],
                        _building_list_building_list_component__WEBPACK_IMPORTED_MODULE_6__["BuildingListComponent"],
                        _pages_building_page_building_page_component__WEBPACK_IMPORTED_MODULE_7__["BuildingPageComponent"],
                        _pages_page_not_found_page_not_found_component__WEBPACK_IMPORTED_MODULE_8__["PageNotFoundComponent"],
                        _forms_edit_building_edit_building_component__WEBPACK_IMPORTED_MODULE_9__["EditBuildingComponent"],
                        _forms_add_building_add_building_component__WEBPACK_IMPORTED_MODULE_11__["AddBuildingComponent"],
                        _pages_elevator_page_elevator_page_component__WEBPACK_IMPORTED_MODULE_12__["ElevatorPageComponent"],
                        _elevator_list_elevator_list_component__WEBPACK_IMPORTED_MODULE_13__["ElevatorListComponent"],
                        _forms_add_elevator_add_elevator_component__WEBPACK_IMPORTED_MODULE_14__["AddElevatorComponent"],
                        _forms_edit_elevator_edit_elevator_component__WEBPACK_IMPORTED_MODULE_15__["EditElevatorComponent"],
                        _forms_ride_elevator_ride_elevator_component__WEBPACK_IMPORTED_MODULE_16__["RideElevatorComponent"],
                        _login_login_component__WEBPACK_IMPORTED_MODULE_17__["LoginComponent"],
                        _pages_home_page_home_page_component__WEBPACK_IMPORTED_MODULE_22__["HomePageComponent"],
                        _pages_about_page_about_page_component__WEBPACK_IMPORTED_MODULE_23__["AboutPageComponent"],
                        _register_register_component__WEBPACK_IMPORTED_MODULE_24__["RegisterComponent"],
                        _search_building_search_component__WEBPACK_IMPORTED_MODULE_25__["SearchComponent"],
                        _search_elevator_filter_filter_component__WEBPACK_IMPORTED_MODULE_26__["FilterComponent"],
                        _comment_list_comment_list_component__WEBPACK_IMPORTED_MODULE_27__["CommentListComponent"]
                    ],
                    imports: [
                        _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
                        _angular_forms__WEBPACK_IMPORTED_MODULE_10__["FormsModule"],
                        _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClientModule"],
                        _app_routing_module__WEBPACK_IMPORTED_MODULE_4__["AppRoutingModule"],
                    ],
                    providers: [
                        {
                            provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HTTP_INTERCEPTORS"],
                            useClass: _service_security_token_interceptor_service__WEBPACK_IMPORTED_MODULE_18__["TokenInterceptorService"],
                            multi: true
                        },
                        _service_security_authentication_service__WEBPACK_IMPORTED_MODULE_19__["AuthenticationService"],
                        _service_security_can_activate_auth_guard_guard__WEBPACK_IMPORTED_MODULE_20__["CanActivateAuthGuardGuard"],
                        _service_security_jwt_utils_service__WEBPACK_IMPORTED_MODULE_21__["JwtUtilsService"]
                    ],
                    bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"]]
                })
            ], AppModule);
            /***/ 
        }),
        /***/ "./src/app/building-list/building-list.component.css": 
        /*!***********************************************************!*\
          !*** ./src/app/building-list/building-list.component.css ***!
          \***********************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = (".custom-btn {\r\n    background-color: teal;\r\n    color: white;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYnVpbGRpbmctbGlzdC9idWlsZGluZy1saXN0LmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxzQkFBc0I7SUFDdEIsWUFBWTtBQUNoQiIsImZpbGUiOiJzcmMvYXBwL2J1aWxkaW5nLWxpc3QvYnVpbGRpbmctbGlzdC5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmN1c3RvbS1idG4ge1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdGVhbDtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxufSJdfQ== */");
            /***/ 
        }),
        /***/ "./src/app/building-list/building-list.component.ts": 
        /*!**********************************************************!*\
          !*** ./src/app/building-list/building-list.component.ts ***!
          \**********************************************************/
        /*! exports provided: BuildingListComponent */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BuildingListComponent", function () { return BuildingListComponent; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            var BuildingListComponent = /** @class */ (function () {
                function BuildingListComponent() {
                    this.markBuildingForEditing = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
                    this.buildingToDelete = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
                    this.markBuildingIdForElevatorForRiding = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
                }
                BuildingListComponent.prototype.ngOnInit = function () {
                };
                BuildingListComponent.prototype.editBuilding = function (building) {
                    this.markBuildingForEditing.emit(building);
                };
                BuildingListComponent.prototype.deleteBuilding = function (id) {
                    this.buildingToDelete.emit(id);
                };
                BuildingListComponent.prototype.rideElevator = function (id) {
                    this.markBuildingIdForElevatorForRiding.emit(id);
                };
                return BuildingListComponent;
            }());
            tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
            ], BuildingListComponent.prototype, "isAdministrator", void 0);
            tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
            ], BuildingListComponent.prototype, "isLoggedIn", void 0);
            tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
            ], BuildingListComponent.prototype, "buildings", void 0);
            tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])()
            ], BuildingListComponent.prototype, "markBuildingForEditing", void 0);
            tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])()
            ], BuildingListComponent.prototype, "buildingToDelete", void 0);
            tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])()
            ], BuildingListComponent.prototype, "markBuildingIdForElevatorForRiding", void 0);
            BuildingListComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
                    selector: 'app-building-list',
                    template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./building-list.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/building-list/building-list.component.html")).default,
                    styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./building-list.component.css */ "./src/app/building-list/building-list.component.css")).default]
                })
            ], BuildingListComponent);
            /***/ 
        }),
        /***/ "./src/app/comment-list/comment-list.component.css": 
        /*!*********************************************************!*\
          !*** ./src/app/comment-list/comment-list.component.css ***!
          \*********************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbW1lbnQtbGlzdC9jb21tZW50LWxpc3QuY29tcG9uZW50LmNzcyJ9 */");
            /***/ 
        }),
        /***/ "./src/app/comment-list/comment-list.component.ts": 
        /*!********************************************************!*\
          !*** ./src/app/comment-list/comment-list.component.ts ***!
          \********************************************************/
        /*! exports provided: CommentListComponent */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CommentListComponent", function () { return CommentListComponent; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _models_comment_model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../models/comment.model */ "./src/app/models/comment.model.ts");
            var CommentListComponent = /** @class */ (function () {
                function CommentListComponent() {
                    this.commentLiked = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
                    this.commentDisliked = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
                    this.commentPosted = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
                }
                CommentListComponent.prototype.ngOnInit = function () {
                    this.resetComment();
                };
                CommentListComponent.prototype.likeComment = function (comment) {
                    this.commentLiked.emit(comment);
                };
                CommentListComponent.prototype.dislikeComment = function (comment) {
                    this.commentDisliked.emit(comment);
                };
                CommentListComponent.prototype.postComment = function () {
                    this.commentPosted.emit(this.newComment);
                    this.resetComment();
                };
                CommentListComponent.prototype.resetComment = function () {
                    this.newComment = new _models_comment_model__WEBPACK_IMPORTED_MODULE_2__["Comment"]({
                        nickname: '',
                        content: ''
                    });
                };
                return CommentListComponent;
            }());
            tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
            ], CommentListComponent.prototype, "comments", void 0);
            tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])()
            ], CommentListComponent.prototype, "commentLiked", void 0);
            tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])()
            ], CommentListComponent.prototype, "commentDisliked", void 0);
            tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])()
            ], CommentListComponent.prototype, "commentPosted", void 0);
            CommentListComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
                    selector: 'app-comment-list',
                    template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./comment-list.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/comment-list/comment-list.component.html")).default,
                    styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./comment-list.component.css */ "./src/app/comment-list/comment-list.component.css")).default]
                })
            ], CommentListComponent);
            /***/ 
        }),
        /***/ "./src/app/elevator-list/elevator-list.component.css": 
        /*!***********************************************************!*\
          !*** ./src/app/elevator-list/elevator-list.component.css ***!
          \***********************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = (".custom-btn {\r\n    background-color: teal;\r\n    color: white;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZWxldmF0b3ItbGlzdC9lbGV2YXRvci1saXN0LmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxzQkFBc0I7SUFDdEIsWUFBWTtBQUNoQiIsImZpbGUiOiJzcmMvYXBwL2VsZXZhdG9yLWxpc3QvZWxldmF0b3ItbGlzdC5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmN1c3RvbS1idG4ge1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdGVhbDtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxufSJdfQ== */");
            /***/ 
        }),
        /***/ "./src/app/elevator-list/elevator-list.component.ts": 
        /*!**********************************************************!*\
          !*** ./src/app/elevator-list/elevator-list.component.ts ***!
          \**********************************************************/
        /*! exports provided: ElevatorListComponent */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ElevatorListComponent", function () { return ElevatorListComponent; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            var ElevatorListComponent = /** @class */ (function () {
                function ElevatorListComponent() {
                    this.markElevatorForEditing = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
                    this.elevatorToDelete = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
                }
                ElevatorListComponent.prototype.ngOnInit = function () {
                };
                ElevatorListComponent.prototype.editElevator = function (elevator) {
                    this.markElevatorForEditing.emit(elevator);
                };
                ElevatorListComponent.prototype.deleteElevator = function (id) {
                    this.elevatorToDelete.emit(id);
                };
                return ElevatorListComponent;
            }());
            tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
            ], ElevatorListComponent.prototype, "isAdministrator", void 0);
            tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
            ], ElevatorListComponent.prototype, "elevators", void 0);
            tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])()
            ], ElevatorListComponent.prototype, "markElevatorForEditing", void 0);
            tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])()
            ], ElevatorListComponent.prototype, "elevatorToDelete", void 0);
            ElevatorListComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
                    selector: 'app-elevator-list',
                    template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./elevator-list.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/elevator-list/elevator-list.component.html")).default,
                    styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./elevator-list.component.css */ "./src/app/elevator-list/elevator-list.component.css")).default]
                })
            ], ElevatorListComponent);
            /***/ 
        }),
        /***/ "./src/app/forms/add-building/add-building.component.css": 
        /*!***************************************************************!*\
          !*** ./src/app/forms/add-building/add-building.component.css ***!
          \***************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = (".container {\r\n    margin-top: 120px;\r\n}\r\n\r\n.custom-btn {\r\n    background-color: teal;\r\n    color: white;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZm9ybXMvYWRkLWJ1aWxkaW5nL2FkZC1idWlsZGluZy5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksaUJBQWlCO0FBQ3JCOztBQUVBO0lBQ0ksc0JBQXNCO0lBQ3RCLFlBQVk7QUFDaEIiLCJmaWxlIjoic3JjL2FwcC9mb3Jtcy9hZGQtYnVpbGRpbmcvYWRkLWJ1aWxkaW5nLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuY29udGFpbmVyIHtcclxuICAgIG1hcmdpbi10b3A6IDEyMHB4O1xyXG59XHJcblxyXG4uY3VzdG9tLWJ0biB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0ZWFsO1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG59Il19 */");
            /***/ 
        }),
        /***/ "./src/app/forms/add-building/add-building.component.ts": 
        /*!**************************************************************!*\
          !*** ./src/app/forms/add-building/add-building.component.ts ***!
          \**************************************************************/
        /*! exports provided: AddBuildingComponent */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddBuildingComponent", function () { return AddBuildingComponent; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var src_app_models_building_model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/models/building.model */ "./src/app/models/building.model.ts");
            /* harmony import */ var src_app_service_buildings_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/service/buildings.service */ "./src/app/service/buildings.service.ts");
            /* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
            var AddBuildingComponent = /** @class */ (function () {
                function AddBuildingComponent(buildingsService, router) {
                    this.buildingsService = buildingsService;
                    this.router = router;
                    this.buildingAdded = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
                }
                AddBuildingComponent.prototype.ngOnInit = function () {
                    this.resetBuildingToAdd();
                };
                // addBuilding() {
                //   this.buildingAdded.emit(this.buildingToAdd);
                //   this.resetBuildingToAdd();
                // }
                AddBuildingComponent.prototype.addBuilding = function (building) {
                    var _this = this;
                    this.buildingsService.addBuilding(building).subscribe(function (res) {
                        // this.loadData();
                        _this.resetBuildingToAdd();
                        _this.router.navigate(["/buildings"]);
                    });
                };
                AddBuildingComponent.prototype.resetBuildingToAdd = function () {
                    this.buildingToAdd = new src_app_models_building_model__WEBPACK_IMPORTED_MODULE_2__["Building"]({
                        name: '',
                        floors: 0
                    });
                };
                return AddBuildingComponent;
            }());
            AddBuildingComponent.ctorParameters = function () { return [
                { type: src_app_service_buildings_service__WEBPACK_IMPORTED_MODULE_3__["BuildingsService"] },
                { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] }
            ]; };
            tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])()
            ], AddBuildingComponent.prototype, "buildingAdded", void 0);
            AddBuildingComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
                    selector: 'app-add-building',
                    template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./add-building.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/forms/add-building/add-building.component.html")).default,
                    styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./add-building.component.css */ "./src/app/forms/add-building/add-building.component.css")).default]
                })
            ], AddBuildingComponent);
            /***/ 
        }),
        /***/ "./src/app/forms/add-elevator/add-elevator.component.css": 
        /*!***************************************************************!*\
          !*** ./src/app/forms/add-elevator/add-elevator.component.css ***!
          \***************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = (".container {\r\n    margin-top: 120px;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZm9ybXMvYWRkLWVsZXZhdG9yL2FkZC1lbGV2YXRvci5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksaUJBQWlCO0FBQ3JCIiwiZmlsZSI6InNyYy9hcHAvZm9ybXMvYWRkLWVsZXZhdG9yL2FkZC1lbGV2YXRvci5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmNvbnRhaW5lciB7XHJcbiAgICBtYXJnaW4tdG9wOiAxMjBweDtcclxufSJdfQ== */");
            /***/ 
        }),
        /***/ "./src/app/forms/add-elevator/add-elevator.component.ts": 
        /*!**************************************************************!*\
          !*** ./src/app/forms/add-elevator/add-elevator.component.ts ***!
          \**************************************************************/
        /*! exports provided: AddElevatorComponent */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddElevatorComponent", function () { return AddElevatorComponent; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var src_app_models_elevator_model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/models/elevator.model */ "./src/app/models/elevator.model.ts");
            /* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
            /* harmony import */ var src_app_service_elevator_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/service/elevator.service */ "./src/app/service/elevator.service.ts");
            var AddElevatorComponent = /** @class */ (function () {
                function AddElevatorComponent(elevatorService, router, route) {
                    this.elevatorService = elevatorService;
                    this.router = router;
                    this.route = route;
                    this.elevatorAdded = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
                }
                AddElevatorComponent.prototype.ngOnInit = function () {
                    this.resetElevatorToAdd();
                };
                AddElevatorComponent.prototype.addElevator = function (elevator) {
                    var _this = this;
                    this.route.params.subscribe(function (param) {
                        _this.id = param.id;
                        _this.elevatorService.addElevator(elevator, _this.id)
                            .subscribe(function (res) {
                            _this.resetElevatorToAdd();
                            _this.router.navigate(["/buildings/" + _this.id + "/elevators"]);
                        });
                    });
                };
                AddElevatorComponent.prototype.resetElevatorToAdd = function () {
                    this.elevatorToAdd = new src_app_models_elevator_model__WEBPACK_IMPORTED_MODULE_2__["Elevator"]({
                        id: 0,
                        code: 0,
                        currentFloor: 0,
                        floorsLeft: 0,
                        building: null,
                        working: false
                    });
                };
                return AddElevatorComponent;
            }());
            AddElevatorComponent.ctorParameters = function () { return [
                { type: src_app_service_elevator_service__WEBPACK_IMPORTED_MODULE_4__["ElevatorService"] },
                { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
                { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] }
            ]; };
            tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])()
            ], AddElevatorComponent.prototype, "elevatorAdded", void 0);
            AddElevatorComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
                    selector: 'app-add-elevator',
                    template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./add-elevator.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/forms/add-elevator/add-elevator.component.html")).default,
                    styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./add-elevator.component.css */ "./src/app/forms/add-elevator/add-elevator.component.css")).default]
                })
            ], AddElevatorComponent);
            /***/ 
        }),
        /***/ "./src/app/forms/edit-building/edit-building.component.css": 
        /*!*****************************************************************!*\
          !*** ./src/app/forms/edit-building/edit-building.component.css ***!
          \*****************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2Zvcm1zL2VkaXQtYnVpbGRpbmcvZWRpdC1idWlsZGluZy5jb21wb25lbnQuY3NzIn0= */");
            /***/ 
        }),
        /***/ "./src/app/forms/edit-building/edit-building.component.ts": 
        /*!****************************************************************!*\
          !*** ./src/app/forms/edit-building/edit-building.component.ts ***!
          \****************************************************************/
        /*! exports provided: EditBuildingComponent */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditBuildingComponent", function () { return EditBuildingComponent; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            var EditBuildingComponent = /** @class */ (function () {
                function EditBuildingComponent() {
                    this.buildingEdited = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
                }
                EditBuildingComponent.prototype.ngOnInit = function () {
                };
                EditBuildingComponent.prototype.editBuilding = function () {
                    this.buildingEdited.emit(this.buildingToEdit);
                };
                return EditBuildingComponent;
            }());
            tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
            ], EditBuildingComponent.prototype, "buildingToEdit", void 0);
            tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])()
            ], EditBuildingComponent.prototype, "buildingEdited", void 0);
            EditBuildingComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
                    selector: 'app-edit-building',
                    template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./edit-building.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/forms/edit-building/edit-building.component.html")).default,
                    styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./edit-building.component.css */ "./src/app/forms/edit-building/edit-building.component.css")).default]
                })
            ], EditBuildingComponent);
            /***/ 
        }),
        /***/ "./src/app/forms/edit-elevator/edit-elevator.component.css": 
        /*!*****************************************************************!*\
          !*** ./src/app/forms/edit-elevator/edit-elevator.component.css ***!
          \*****************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2Zvcm1zL2VkaXQtZWxldmF0b3IvZWRpdC1lbGV2YXRvci5jb21wb25lbnQuY3NzIn0= */");
            /***/ 
        }),
        /***/ "./src/app/forms/edit-elevator/edit-elevator.component.ts": 
        /*!****************************************************************!*\
          !*** ./src/app/forms/edit-elevator/edit-elevator.component.ts ***!
          \****************************************************************/
        /*! exports provided: EditElevatorComponent */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditElevatorComponent", function () { return EditElevatorComponent; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            var EditElevatorComponent = /** @class */ (function () {
                function EditElevatorComponent() {
                    this.elevatorEdited = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
                }
                EditElevatorComponent.prototype.ngOnInit = function () {
                };
                EditElevatorComponent.prototype.editElevator = function () {
                    this.elevatorEdited.emit(this.elevatorToEdit);
                };
                return EditElevatorComponent;
            }());
            tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
            ], EditElevatorComponent.prototype, "isAdministrator", void 0);
            tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
            ], EditElevatorComponent.prototype, "elevatorToEdit", void 0);
            tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])()
            ], EditElevatorComponent.prototype, "elevatorEdited", void 0);
            EditElevatorComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
                    selector: 'app-edit-elevator',
                    template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./edit-elevator.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/forms/edit-elevator/edit-elevator.component.html")).default,
                    styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./edit-elevator.component.css */ "./src/app/forms/edit-elevator/edit-elevator.component.css")).default]
                })
            ], EditElevatorComponent);
            /***/ 
        }),
        /***/ "./src/app/forms/ride-elevator/ride-elevator.component.css": 
        /*!*****************************************************************!*\
          !*** ./src/app/forms/ride-elevator/ride-elevator.component.css ***!
          \*****************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = (".custom-btn {\r\n    background-color: cadetblue;\r\n    color: white;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZm9ybXMvcmlkZS1lbGV2YXRvci9yaWRlLWVsZXZhdG9yLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSwyQkFBMkI7SUFDM0IsWUFBWTtBQUNoQiIsImZpbGUiOiJzcmMvYXBwL2Zvcm1zL3JpZGUtZWxldmF0b3IvcmlkZS1lbGV2YXRvci5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmN1c3RvbS1idG4ge1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogY2FkZXRibHVlO1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG59Il19 */");
            /***/ 
        }),
        /***/ "./src/app/forms/ride-elevator/ride-elevator.component.ts": 
        /*!****************************************************************!*\
          !*** ./src/app/forms/ride-elevator/ride-elevator.component.ts ***!
          \****************************************************************/
        /*! exports provided: RideElevatorComponent */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RideElevatorComponent", function () { return RideElevatorComponent; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var src_app_models_rideElevator_model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/models/rideElevator.model */ "./src/app/models/rideElevator.model.ts");
            var RideElevatorComponent = /** @class */ (function () {
                function RideElevatorComponent() {
                    this.elevatorRided = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
                }
                RideElevatorComponent.prototype.ngOnInit = function () {
                    this.resetRideElevatorForm();
                };
                RideElevatorComponent.prototype.rideElevator = function () {
                    this.elevatorToRide.buildingId = this.buildingId;
                    this.elevatorRided.emit(this.elevatorToRide);
                    this.resetRideElevatorForm();
                };
                RideElevatorComponent.prototype.resetRideElevatorForm = function () {
                    this.elevatorToRide = new src_app_models_rideElevator_model__WEBPACK_IMPORTED_MODULE_2__["RideElevator"]({
                        buildingId: 0,
                        startFloor: 0,
                        endFloor: 0
                    });
                };
                return RideElevatorComponent;
            }());
            tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])()
            ], RideElevatorComponent.prototype, "elevatorRided", void 0);
            tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
            ], RideElevatorComponent.prototype, "buildingId", void 0);
            RideElevatorComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
                    selector: 'app-ride-elevator',
                    template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./ride-elevator.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/forms/ride-elevator/ride-elevator.component.html")).default,
                    styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./ride-elevator.component.css */ "./src/app/forms/ride-elevator/ride-elevator.component.css")).default]
                })
            ], RideElevatorComponent);
            /***/ 
        }),
        /***/ "./src/app/login/login.component.css": 
        /*!*******************************************!*\
          !*** ./src/app/login/login.component.css ***!
          \*******************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2xvZ2luL2xvZ2luLmNvbXBvbmVudC5jc3MifQ== */");
            /***/ 
        }),
        /***/ "./src/app/login/login.component.ts": 
        /*!******************************************!*\
          !*** ./src/app/login/login.component.ts ***!
          \******************************************/
        /*! exports provided: LoginComponent */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function () { return LoginComponent; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _service_security_authentication_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../service/security/authentication.service */ "./src/app/service/security/authentication.service.ts");
            /* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
            /* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
            var LoginComponent = /** @class */ (function () {
                function LoginComponent(authenticationService, router, location) {
                    this.authenticationService = authenticationService;
                    this.router = router;
                    this.location = location;
                    this.user = {};
                    this.wrongUsernameOrPass = false;
                }
                LoginComponent.prototype.ngOnInit = function () {
                };
                LoginComponent.prototype.login = function () {
                    var _this = this;
                    this.authenticationService.login(this.user.username, this.user.password).subscribe(function (loggedIn) {
                        if (loggedIn) {
                            _this.router.navigate(["/home"]);
                        }
                    });
                };
                return LoginComponent;
            }());
            LoginComponent.ctorParameters = function () { return [
                { type: _service_security_authentication_service__WEBPACK_IMPORTED_MODULE_2__["AuthenticationService"] },
                { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
                { type: _angular_common__WEBPACK_IMPORTED_MODULE_4__["Location"] }
            ]; };
            LoginComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
                    selector: 'app-login',
                    template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./login.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/login/login.component.html")).default,
                    encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewEncapsulation"].None,
                    styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./login.component.css */ "./src/app/login/login.component.css")).default]
                })
            ], LoginComponent);
            /***/ 
        }),
        /***/ "./src/app/models/building.model.ts": 
        /*!******************************************!*\
          !*** ./src/app/models/building.model.ts ***!
          \******************************************/
        /*! exports provided: Building */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Building", function () { return Building; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            var Building = /** @class */ (function () {
                function Building(buildingCfg) {
                    this.id = buildingCfg.id;
                    this.name = buildingCfg.name;
                    this.floors = buildingCfg.floors;
                    this.elevators = buildingCfg.elevators;
                }
                return Building;
            }());
            /***/ 
        }),
        /***/ "./src/app/models/comment.model.ts": 
        /*!*****************************************!*\
          !*** ./src/app/models/comment.model.ts ***!
          \*****************************************/
        /*! exports provided: Comment */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Comment", function () { return Comment; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            var Comment = /** @class */ (function () {
                function Comment(commentCfg) {
                    this.id = commentCfg.id;
                    this.nickname = commentCfg.nickname;
                    this.content = commentCfg.content;
                    this.likes = commentCfg.likes;
                    this.dislikes = commentCfg.dislikes;
                }
                return Comment;
            }());
            /***/ 
        }),
        /***/ "./src/app/models/elevator.model.ts": 
        /*!******************************************!*\
          !*** ./src/app/models/elevator.model.ts ***!
          \******************************************/
        /*! exports provided: Elevator */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Elevator", function () { return Elevator; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            var Elevator = /** @class */ (function () {
                function Elevator(elevatorCfg) {
                    this.id = elevatorCfg.id;
                    this.code = elevatorCfg.code;
                    this.currentFloor = elevatorCfg.currentFloor;
                    this.floorsLeft = elevatorCfg.floorsLeft;
                    this.working = elevatorCfg.working;
                    this.building = elevatorCfg.building;
                }
                Elevator.prototype.decreeseFloorsLeft = function (number) {
                    var remainingFloors = this.floorsLeft - number;
                    this.floorsLeft = remainingFloors < 0 ? 0 : remainingFloors;
                };
                return Elevator;
            }());
            /***/ 
        }),
        /***/ "./src/app/models/rideElevator.model.ts": 
        /*!**********************************************!*\
          !*** ./src/app/models/rideElevator.model.ts ***!
          \**********************************************/
        /*! exports provided: RideElevator */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RideElevator", function () { return RideElevator; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            var RideElevator = /** @class */ (function () {
                function RideElevator(rideElevatorCfg) {
                    this.buildingId = rideElevatorCfg.buildingId;
                    this.startFloor = rideElevatorCfg.startFloor;
                    this.endFloor = rideElevatorCfg.endFloor;
                }
                return RideElevator;
            }());
            /***/ 
        }),
        /***/ "./src/app/pages/about-page/about-page.component.css": 
        /*!***********************************************************!*\
          !*** ./src/app/pages/about-page/about-page.component.css ***!
          \***********************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2Fib3V0LXBhZ2UvYWJvdXQtcGFnZS5jb21wb25lbnQuY3NzIn0= */");
            /***/ 
        }),
        /***/ "./src/app/pages/about-page/about-page.component.ts": 
        /*!**********************************************************!*\
          !*** ./src/app/pages/about-page/about-page.component.ts ***!
          \**********************************************************/
        /*! exports provided: AboutPageComponent */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AboutPageComponent", function () { return AboutPageComponent; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            var AboutPageComponent = /** @class */ (function () {
                function AboutPageComponent() {
                }
                AboutPageComponent.prototype.ngOnInit = function () {
                };
                return AboutPageComponent;
            }());
            AboutPageComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
                    selector: 'app-about-page',
                    template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./about-page.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/about-page/about-page.component.html")).default,
                    styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./about-page.component.css */ "./src/app/pages/about-page/about-page.component.css")).default]
                })
            ], AboutPageComponent);
            /***/ 
        }),
        /***/ "./src/app/pages/building-page/building-page.component.css": 
        /*!*****************************************************************!*\
          !*** ./src/app/pages/building-page/building-page.component.css ***!
          \*****************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2J1aWxkaW5nLXBhZ2UvYnVpbGRpbmctcGFnZS5jb21wb25lbnQuY3NzIn0= */");
            /***/ 
        }),
        /***/ "./src/app/pages/building-page/building-page.component.ts": 
        /*!****************************************************************!*\
          !*** ./src/app/pages/building-page/building-page.component.ts ***!
          \****************************************************************/
        /*! exports provided: BuildingPageComponent */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BuildingPageComponent", function () { return BuildingPageComponent; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var src_app_models_building_model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/models/building.model */ "./src/app/models/building.model.ts");
            /* harmony import */ var src_app_service_buildings_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/service/buildings.service */ "./src/app/service/buildings.service.ts");
            /* harmony import */ var src_app_service_elevator_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/service/elevator.service */ "./src/app/service/elevator.service.ts");
            /* harmony import */ var src_app_service_security_authentication_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/service/security/authentication.service */ "./src/app/service/security/authentication.service.ts");
            var BuildingPageComponent = /** @class */ (function () {
                function BuildingPageComponent(buildingsService, authenticationService, elevatorService) {
                    this.buildingsService = buildingsService;
                    this.authenticationService = authenticationService;
                    this.elevatorService = elevatorService;
                    this.page = 0;
                    this.buildingName = "";
                }
                BuildingPageComponent.prototype.ngOnInit = function () {
                    this.isAdministrator = this.authenticationService.getCurrentUser() ?
                        this.authenticationService.getCurrentUser().roles.indexOf('ROLE_ADMIN') !== -1 : false;
                    this.isLoggedIn = this.authenticationService.isLoggedIn();
                    this.resetActiveBuilding();
                    this.loadData();
                };
                BuildingPageComponent.prototype.deleteBuilding = function (id) {
                    var _this = this;
                    this.buildingsService.deleteBuilding(id).subscribe(function (res) {
                        _this.loadData();
                    });
                };
                BuildingPageComponent.prototype.editBuilding = function (buiding) {
                    var _this = this;
                    this.buildingsService.editBuilding(buiding).subscribe(function (res) {
                        _this.loadData();
                        _this.resetActiveBuilding();
                    });
                };
                BuildingPageComponent.prototype.loadData = function () {
                    var _this = this;
                    this.buildingsService.getBuildings(this.page, this.buildingName).subscribe(function (res) {
                        _this.buildings = res;
                        _this.loadComments();
                    });
                };
                BuildingPageComponent.prototype.loadComments = function () {
                    var _this = this;
                    this.buildingsService.getComments().subscribe(function (res) {
                        _this.comments = res;
                    });
                };
                BuildingPageComponent.prototype.likeComment = function (comment) {
                    var _this = this;
                    this.buildingsService.likeComment(comment)
                        .subscribe(function (res) {
                        _this.loadComments();
                    });
                };
                BuildingPageComponent.prototype.dislikeComment = function (comment) {
                    var _this = this;
                    this.buildingsService.dislikeComment(comment)
                        .subscribe(function (res) {
                        _this.loadComments();
                    });
                };
                BuildingPageComponent.prototype.postComment = function (comment) {
                    var _this = this;
                    this.buildingsService.postComment(comment).subscribe(function (res) {
                        _this.loadComments();
                    });
                };
                BuildingPageComponent.prototype.resetCommentForm = function () {
                };
                BuildingPageComponent.prototype.setActiveBuilding = function (building) {
                    this.activeBuilding = new src_app_models_building_model__WEBPACK_IMPORTED_MODULE_2__["Building"](building);
                };
                BuildingPageComponent.prototype.setBuildingIdForElevatorRiding = function (id) {
                    this.activeBuildingId = id;
                };
                BuildingPageComponent.prototype.resetActiveBuilding = function () {
                    this.activeBuilding = new src_app_models_building_model__WEBPACK_IMPORTED_MODULE_2__["Building"]({
                        name: '',
                        floors: 0,
                        elevators: null
                    });
                };
                BuildingPageComponent.prototype.rideElevator = function (rideElevator) {
                    this.elevatorService.rideElevator(rideElevator)
                        .subscribe(function (res) {
                    });
                };
                BuildingPageComponent.prototype.search = function (searchTerm) {
                    var _this = this;
                    this.buildingsService.getBuildings(this.page, searchTerm)
                        .subscribe(function (buidings) {
                        _this.buildings = buidings;
                    });
                };
                BuildingPageComponent.prototype.nextPage = function () {
                    this.page += 1;
                    this.loadData();
                };
                BuildingPageComponent.prototype.prevPage = function () {
                    if (this.page > 0) {
                        this.page -= 1;
                        this.loadData();
                    }
                };
                BuildingPageComponent.prototype.callPage = function (pageNumber) {
                    this.page = pageNumber;
                    this.loadData();
                };
                return BuildingPageComponent;
            }());
            BuildingPageComponent.ctorParameters = function () { return [
                { type: src_app_service_buildings_service__WEBPACK_IMPORTED_MODULE_3__["BuildingsService"] },
                { type: src_app_service_security_authentication_service__WEBPACK_IMPORTED_MODULE_5__["AuthenticationService"] },
                { type: src_app_service_elevator_service__WEBPACK_IMPORTED_MODULE_4__["ElevatorService"] }
            ]; };
            BuildingPageComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
                    selector: 'app-building-page',
                    template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./building-page.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/building-page/building-page.component.html")).default,
                    styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./building-page.component.css */ "./src/app/pages/building-page/building-page.component.css")).default]
                })
            ], BuildingPageComponent);
            /***/ 
        }),
        /***/ "./src/app/pages/elevator-page/elevator-page.component.css": 
        /*!*****************************************************************!*\
          !*** ./src/app/pages/elevator-page/elevator-page.component.css ***!
          \*****************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2VsZXZhdG9yLXBhZ2UvZWxldmF0b3ItcGFnZS5jb21wb25lbnQuY3NzIn0= */");
            /***/ 
        }),
        /***/ "./src/app/pages/elevator-page/elevator-page.component.ts": 
        /*!****************************************************************!*\
          !*** ./src/app/pages/elevator-page/elevator-page.component.ts ***!
          \****************************************************************/
        /*! exports provided: ElevatorPageComponent */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ElevatorPageComponent", function () { return ElevatorPageComponent; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var src_app_models_elevator_model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/models/elevator.model */ "./src/app/models/elevator.model.ts");
            /* harmony import */ var src_app_service_elevator_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/service/elevator.service */ "./src/app/service/elevator.service.ts");
            /* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
            /* harmony import */ var src_app_service_security_authentication_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/service/security/authentication.service */ "./src/app/service/security/authentication.service.ts");
            var ElevatorPageComponent = /** @class */ (function () {
                function ElevatorPageComponent(elevatorService, route, authenticationService) {
                    this.elevatorService = elevatorService;
                    this.route = route;
                    this.authenticationService = authenticationService;
                    this.page = 0;
                }
                ElevatorPageComponent.prototype.ngOnInit = function () {
                    //bitno da ne dobijamo gresku undefined pri ucitavanju
                    this.isAdministrator = this.authenticationService.getCurrentUser() ?
                        this.authenticationService.getCurrentUser().roles.indexOf('ROLE_ADMIN') !== -1 : false;
                    this.resetActiveElevator();
                    this.loadData();
                };
                ElevatorPageComponent.prototype.loadData = function () {
                    var _this = this;
                    this.route.params.subscribe(function (param) {
                        _this.id = param.id;
                        _this.elevatorService.getInBuildingAndCodeOrWorking(_this.id, _this.code, _this.working, _this.page)
                            .subscribe(function (res) {
                            _this.elevators = res;
                        });
                    });
                };
                ElevatorPageComponent.prototype.addElevator = function (elevator) {
                    var _this = this;
                    this.route.params.subscribe(function (param) {
                        _this.id = param.id;
                        _this.elevatorService.addElevator(elevator, _this.id)
                            .subscribe(function (res) {
                            _this.loadData();
                        });
                    });
                };
                ElevatorPageComponent.prototype.editElevator = function (elevator) {
                    var _this = this;
                    this.elevatorService.editElevator(elevator).subscribe(function (res) {
                        _this.loadData();
                        _this.resetActiveElevator();
                    });
                };
                ElevatorPageComponent.prototype.deleteElevator = function (id) {
                    var _this = this;
                    this.elevatorService.deleteElevator(id).subscribe(function (res) {
                        _this.loadData();
                    });
                };
                ElevatorPageComponent.prototype.resetActiveElevator = function () {
                    this.activeElevator = new src_app_models_elevator_model__WEBPACK_IMPORTED_MODULE_2__["Elevator"]({
                        id: 0,
                        building: null,
                        code: 0,
                        currentFloor: 0,
                        floorsLeft: 0,
                        working: false
                    });
                };
                ElevatorPageComponent.prototype.setActiveElevator = function (elevator) {
                    this.activeElevator = new src_app_models_elevator_model__WEBPACK_IMPORTED_MODULE_2__["Elevator"](elevator);
                };
                ElevatorPageComponent.prototype.setCode = function (code) {
                    this.code = code;
                    this.loadData();
                };
                ElevatorPageComponent.prototype.setWorking = function (working) {
                    this.working = working;
                    this.loadData();
                };
                ElevatorPageComponent.prototype.resetFilter = function () {
                    this.code = undefined;
                    this.working = undefined;
                    this.loadData();
                };
                ElevatorPageComponent.prototype.nextPage = function () {
                    this.page += 1;
                    this.loadData();
                };
                ElevatorPageComponent.prototype.prevPage = function () {
                    if (this.page > 0) {
                        this.page -= 1;
                        this.loadData();
                    }
                };
                ElevatorPageComponent.prototype.callPage = function (pageNumber) {
                    this.page = pageNumber;
                    this.loadData();
                };
                return ElevatorPageComponent;
            }());
            ElevatorPageComponent.ctorParameters = function () { return [
                { type: src_app_service_elevator_service__WEBPACK_IMPORTED_MODULE_3__["ElevatorService"] },
                { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"] },
                { type: src_app_service_security_authentication_service__WEBPACK_IMPORTED_MODULE_5__["AuthenticationService"] }
            ]; };
            ElevatorPageComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
                    selector: 'app-elevator-page',
                    template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./elevator-page.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/elevator-page/elevator-page.component.html")).default,
                    styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./elevator-page.component.css */ "./src/app/pages/elevator-page/elevator-page.component.css")).default]
                })
            ], ElevatorPageComponent);
            /***/ 
        }),
        /***/ "./src/app/pages/home-page/home-page.component.css": 
        /*!*********************************************************!*\
          !*** ./src/app/pages/home-page/home-page.component.css ***!
          \*********************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("\r\nimg{\r\n    max-width: 100%;\r\n}\r\n\r\n.card-img-top {\r\n    width: 120px;\r\n    height: 120px;\r\n}\r\n\r\n.custom-btn {\r\n    background-color: teal;\r\n    color: white;\r\n}\r\n\r\n@media only screen and (max-width: 768px) {\r\n    .custom-img {\r\n        margin: 200px;\r\n        height: 100%;\r\n        width: 100%;\r\n    }\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvaG9tZS1wYWdlL2hvbWUtcGFnZS5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFDQTtJQUNJLGVBQWU7QUFDbkI7O0FBRUE7SUFDSSxZQUFZO0lBQ1osYUFBYTtBQUNqQjs7QUFFQTtJQUNJLHNCQUFzQjtJQUN0QixZQUFZO0FBQ2hCOztBQUVBO0lBQ0k7UUFDSSxhQUFhO1FBQ2IsWUFBWTtRQUNaLFdBQVc7SUFDZjtBQUNKIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvaG9tZS1wYWdlL2hvbWUtcGFnZS5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiXHJcbmltZ3tcclxuICAgIG1heC13aWR0aDogMTAwJTtcclxufVxyXG5cclxuLmNhcmQtaW1nLXRvcCB7XHJcbiAgICB3aWR0aDogMTIwcHg7XHJcbiAgICBoZWlnaHQ6IDEyMHB4O1xyXG59XHJcblxyXG4uY3VzdG9tLWJ0biB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0ZWFsO1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG59XHJcblxyXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDc2OHB4KSB7XHJcbiAgICAuY3VzdG9tLWltZyB7XHJcbiAgICAgICAgbWFyZ2luOiAyMDBweDtcclxuICAgICAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICB9XHJcbn0iXX0= */");
            /***/ 
        }),
        /***/ "./src/app/pages/home-page/home-page.component.ts": 
        /*!********************************************************!*\
          !*** ./src/app/pages/home-page/home-page.component.ts ***!
          \********************************************************/
        /*! exports provided: HomePageComponent */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageComponent", function () { return HomePageComponent; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            var HomePageComponent = /** @class */ (function () {
                function HomePageComponent() {
                }
                HomePageComponent.prototype.ngOnInit = function () {
                };
                return HomePageComponent;
            }());
            HomePageComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
                    selector: 'app-home-page',
                    template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./home-page.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/home-page/home-page.component.html")).default,
                    styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./home-page.component.css */ "./src/app/pages/home-page/home-page.component.css")).default]
                })
            ], HomePageComponent);
            /***/ 
        }),
        /***/ "./src/app/pages/page-not-found/page-not-found.component.css": 
        /*!*******************************************************************!*\
          !*** ./src/app/pages/page-not-found/page-not-found.component.css ***!
          \*******************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3BhZ2Utbm90LWZvdW5kL3BhZ2Utbm90LWZvdW5kLmNvbXBvbmVudC5jc3MifQ== */");
            /***/ 
        }),
        /***/ "./src/app/pages/page-not-found/page-not-found.component.ts": 
        /*!******************************************************************!*\
          !*** ./src/app/pages/page-not-found/page-not-found.component.ts ***!
          \******************************************************************/
        /*! exports provided: PageNotFoundComponent */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PageNotFoundComponent", function () { return PageNotFoundComponent; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            var PageNotFoundComponent = /** @class */ (function () {
                function PageNotFoundComponent() {
                }
                PageNotFoundComponent.prototype.ngOnInit = function () {
                };
                return PageNotFoundComponent;
            }());
            PageNotFoundComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
                    selector: 'app-page-not-found',
                    template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./page-not-found.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/page-not-found/page-not-found.component.html")).default,
                    styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./page-not-found.component.css */ "./src/app/pages/page-not-found/page-not-found.component.css")).default]
                })
            ], PageNotFoundComponent);
            /***/ 
        }),
        /***/ "./src/app/register/register.component.css": 
        /*!*************************************************!*\
          !*** ./src/app/register/register.component.css ***!
          \*************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3JlZ2lzdGVyL3JlZ2lzdGVyLmNvbXBvbmVudC5jc3MifQ== */");
            /***/ 
        }),
        /***/ "./src/app/register/register.component.ts": 
        /*!************************************************!*\
          !*** ./src/app/register/register.component.ts ***!
          \************************************************/
        /*! exports provided: RegisterComponent */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisterComponent", function () { return RegisterComponent; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _service_security_authentication_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../service/security/authentication.service */ "./src/app/service/security/authentication.service.ts");
            /* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
            var RegisterComponent = /** @class */ (function () {
                function RegisterComponent(authS, router) {
                    this.authS = authS;
                    this.router = router;
                    this.user = {};
                    this.usernameExists = false;
                }
                RegisterComponent.prototype.ngOnInit = function () {
                };
                RegisterComponent.prototype.register = function () {
                    var _this = this;
                    this.authS.register(this.user.firstName, this.user.lastName, this.user.username, this.user.password)
                        .subscribe(function (isRegistered) {
                        if (isRegistered) {
                            _this.router.navigate(["/login"]);
                        }
                    });
                };
                return RegisterComponent;
            }());
            RegisterComponent.ctorParameters = function () { return [
                { type: _service_security_authentication_service__WEBPACK_IMPORTED_MODULE_2__["AuthenticationService"] },
                { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] }
            ]; };
            RegisterComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
                    selector: 'app-register',
                    template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./register.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/register/register.component.html")).default,
                    styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./register.component.css */ "./src/app/register/register.component.css")).default]
                })
            ], RegisterComponent);
            /***/ 
        }),
        /***/ "./src/app/search/building/search.component.css": 
        /*!******************************************************!*\
          !*** ./src/app/search/building/search.component.css ***!
          \******************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NlYXJjaC9idWlsZGluZy9zZWFyY2guY29tcG9uZW50LmNzcyJ9 */");
            /***/ 
        }),
        /***/ "./src/app/search/building/search.component.ts": 
        /*!*****************************************************!*\
          !*** ./src/app/search/building/search.component.ts ***!
          \*****************************************************/
        /*! exports provided: SearchComponent */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchComponent", function () { return SearchComponent; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            var SearchComponent = /** @class */ (function () {
                function SearchComponent() {
                    this.setSearchTerm = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
                    this.searchTerm = "";
                }
                SearchComponent.prototype.ngOnInit = function () {
                };
                SearchComponent.prototype.search = function () {
                    this.setSearchTerm.emit(this.searchTerm);
                };
                SearchComponent.prototype.reset = function () {
                    this.searchTerm = "";
                    this.setSearchTerm.emit(this.searchTerm);
                };
                return SearchComponent;
            }());
            tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])()
            ], SearchComponent.prototype, "setSearchTerm", void 0);
            SearchComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
                    selector: 'app-search',
                    template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./search.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/search/building/search.component.html")).default,
                    styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./search.component.css */ "./src/app/search/building/search.component.css")).default]
                })
            ], SearchComponent);
            /***/ 
        }),
        /***/ "./src/app/search/elevator/filter/filter.component.css": 
        /*!*************************************************************!*\
          !*** ./src/app/search/elevator/filter/filter.component.css ***!
          \*************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NlYXJjaC9lbGV2YXRvci9maWx0ZXIvZmlsdGVyLmNvbXBvbmVudC5jc3MifQ== */");
            /***/ 
        }),
        /***/ "./src/app/search/elevator/filter/filter.component.ts": 
        /*!************************************************************!*\
          !*** ./src/app/search/elevator/filter/filter.component.ts ***!
          \************************************************************/
        /*! exports provided: FilterComponent */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FilterComponent", function () { return FilterComponent; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            var FilterComponent = /** @class */ (function () {
                function FilterComponent() {
                    this.setCode = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
                    this.setWorking = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
                    this.code = undefined;
                    this.working = undefined;
                }
                FilterComponent.prototype.ngOnInit = function () {
                };
                FilterComponent.prototype.filter = function () {
                    this.setCode.emit(this.code);
                    this.setWorking.emit(this.working);
                };
                FilterComponent.prototype.reset = function () {
                    this.code = undefined;
                    this.working = undefined;
                    this.setCode.emit(this.code);
                    this.setWorking.emit(this.working);
                };
                return FilterComponent;
            }());
            tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])()
            ], FilterComponent.prototype, "setCode", void 0);
            tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])()
            ], FilterComponent.prototype, "setWorking", void 0);
            FilterComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
                    selector: 'app-filter',
                    template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./filter.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/search/elevator/filter/filter.component.html")).default,
                    styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./filter.component.css */ "./src/app/search/elevator/filter/filter.component.css")).default]
                })
            ], FilterComponent);
            /***/ 
        }),
        /***/ "./src/app/service/buildings.service.ts": 
        /*!**********************************************!*\
          !*** ./src/app/service/buildings.service.ts ***!
          \**********************************************/
        /*! exports provided: BuildingsService */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BuildingsService", function () { return BuildingsService; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
            var BuildingsService = /** @class */ (function () {
                function BuildingsService(http) {
                    this.http = http;
                    this.path = 'api/buildings';
                    this.pageSize = 5;
                }
                BuildingsService.prototype.getBuildings = function (page, name) {
                    return this.http.get(this.path + ("?page=" + page + "&size=" + this.pageSize + "&name=" + name));
                };
                BuildingsService.prototype.deleteBuilding = function (id) {
                    return this.http.delete(this.path + "/" + id);
                };
                BuildingsService.prototype.editBuilding = function (building) {
                    return this.http.put(this.path + "/" + building.id, building);
                };
                BuildingsService.prototype.addBuilding = function (building) {
                    return this.http.post("" + this.path, building);
                };
                BuildingsService.prototype.getComments = function () {
                    return this.http.get("api/buildings/comments");
                };
                BuildingsService.prototype.postComment = function (comment) {
                    return this.http.post("api/buildings/comments", comment);
                };
                BuildingsService.prototype.likeComment = function (comment) {
                    return this.http.put("api/buildings/comments/" + comment.id + "/like", comment);
                };
                BuildingsService.prototype.dislikeComment = function (comment) {
                    return this.http.put("api/buildings/comments/" + comment.id + "/dislike", comment);
                };
                return BuildingsService;
            }());
            BuildingsService.ctorParameters = function () { return [
                { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
            ]; };
            BuildingsService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
                    providedIn: 'root'
                })
            ], BuildingsService);
            /***/ 
        }),
        /***/ "./src/app/service/elevator.service.ts": 
        /*!*********************************************!*\
          !*** ./src/app/service/elevator.service.ts ***!
          \*********************************************/
        /*! exports provided: ElevatorService */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ElevatorService", function () { return ElevatorService; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
            var ElevatorService = /** @class */ (function () {
                function ElevatorService(http) {
                    this.http = http;
                    this.path = 'api/elevators';
                    this.pageSize = 5;
                }
                ElevatorService.prototype.getElevators = function () {
                    return this.http.get(this.path);
                };
                ElevatorService.prototype.getElevatorsInBuilding = function (id) {
                    return this.http.get("api/buildings/" + id + "/elevators");
                };
                ElevatorService.prototype.getInBuildingAndCodeOrWorking = function (id, code, working, page) {
                    var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]();
                    if (code !== undefined) {
                        params = params.set('code', code.toString());
                    }
                    if (working !== undefined) {
                        params = params.set('working', working.toString());
                    }
                    return this.http.get("api/buildings/" + id + "/elevators?page=" + page + "&size=" + this.pageSize, { params: params });
                };
                ElevatorService.prototype.editElevator = function (elevator) {
                    return this.http.put("api/elevators/" + elevator.id, elevator);
                };
                ElevatorService.prototype.deleteElevator = function (id) {
                    return this.http.delete(this.path + "/" + id);
                };
                ElevatorService.prototype.addElevator = function (elevator, id) {
                    return this.http.post("api/buildings/" + id + "/elevators", elevator);
                };
                ElevatorService.prototype.rideElevator = function (rideElevator) {
                    return this.http.post("api/rideElevator", rideElevator);
                };
                return ElevatorService;
            }());
            ElevatorService.ctorParameters = function () { return [
                { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
            ]; };
            ElevatorService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
                    providedIn: 'root'
                })
            ], ElevatorService);
            /***/ 
        }),
        /***/ "./src/app/service/security/authentication.service.ts": 
        /*!************************************************************!*\
          !*** ./src/app/service/security/authentication.service.ts ***!
          \************************************************************/
        /*! exports provided: AuthenticationService */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthenticationService", function () { return AuthenticationService; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
            /* harmony import */ var _jwt_utils_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./jwt-utils.service */ "./src/app/service/security/jwt-utils.service.ts");
            /* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
            var AuthenticationService = /** @class */ (function () {
                function AuthenticationService(http, jwtUtilsService) {
                    this.http = http;
                    this.jwtUtilsService = jwtUtilsService;
                    this.loginPath = '/api/login';
                    this.registerPath = '/api/register';
                }
                AuthenticationService.prototype.login = function (username, password) {
                    var _this = this;
                    var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({ 'Content-Type': 'application/json' });
                    return this.http.post(this.loginPath, JSON.stringify({ username: username, password: password }), { headers: headers })
                        .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (res) {
                        var token = res && res['token'];
                        if (token) {
                            localStorage.setItem('currentUser', JSON.stringify({
                                username: username,
                                roles: _this.jwtUtilsService.getRoles(token),
                                token: token
                            }));
                            return true;
                        }
                        else {
                            return false;
                        }
                    }));
                };
                AuthenticationService.prototype.register = function (firstName, lastName, username, password) {
                    var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({ 'Content-Type': 'application/json' });
                    return this.http.post(this.registerPath, JSON.stringify({ firstName: firstName, lastName: lastName, username: username, password: password }), { headers: headers })
                        .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (res) {
                        if (res) {
                            return true;
                        }
                        else {
                            return false;
                        }
                    }));
                };
                AuthenticationService.prototype.getToken = function () {
                    var currentUser = JSON.parse(localStorage.getItem('currentUser'));
                    var token = currentUser && currentUser.token;
                    return token ? token : "";
                };
                AuthenticationService.prototype.logOut = function () {
                    localStorage.removeItem('currentUser');
                };
                AuthenticationService.prototype.isLoggedIn = function () {
                    if (this.getToken() !== '') {
                        return true;
                    }
                    else {
                        return false;
                    }
                };
                AuthenticationService.prototype.getCurrentUser = function () {
                    if (localStorage.currentUser) {
                        return JSON.parse(localStorage.currentUser);
                    }
                    else {
                        return undefined;
                    }
                };
                AuthenticationService.prototype.isAdmin = function () {
                    return this.getCurrentUser() ?
                        this.getCurrentUser().roles.indexOf('ROLE_ADMIN') !== -1 : false;
                };
                return AuthenticationService;
            }());
            AuthenticationService.ctorParameters = function () { return [
                { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] },
                { type: _jwt_utils_service__WEBPACK_IMPORTED_MODULE_3__["JwtUtilsService"] }
            ]; };
            AuthenticationService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()
            ], AuthenticationService);
            /***/ 
        }),
        /***/ "./src/app/service/security/can-activate-auth-guard.guard.ts": 
        /*!*******************************************************************!*\
          !*** ./src/app/service/security/can-activate-auth-guard.guard.ts ***!
          \*******************************************************************/
        /*! exports provided: CanActivateAuthGuardGuard */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CanActivateAuthGuardGuard", function () { return CanActivateAuthGuardGuard; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
            /* harmony import */ var _authentication_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./authentication.service */ "./src/app/service/security/authentication.service.ts");
            var CanActivateAuthGuardGuard = /** @class */ (function () {
                function CanActivateAuthGuardGuard(authService, router) {
                    this.authService = authService;
                    this.router = router;
                }
                CanActivateAuthGuardGuard.prototype.canActivate = function (next, state) {
                    if (this.authService.isLoggedIn() && this.authService.isAdmin()) {
                        return true;
                    }
                    else {
                        this.router.navigate(['/buildings']);
                        return false;
                    }
                };
                return CanActivateAuthGuardGuard;
            }());
            CanActivateAuthGuardGuard.ctorParameters = function () { return [
                { type: _authentication_service__WEBPACK_IMPORTED_MODULE_3__["AuthenticationService"] },
                { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
            ]; };
            CanActivateAuthGuardGuard = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
                    providedIn: 'root'
                })
            ], CanActivateAuthGuardGuard);
            /***/ 
        }),
        /***/ "./src/app/service/security/jwt-utils.service.ts": 
        /*!*******************************************************!*\
          !*** ./src/app/service/security/jwt-utils.service.ts ***!
          \*******************************************************/
        /*! exports provided: JwtUtilsService */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "JwtUtilsService", function () { return JwtUtilsService; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            var JwtUtilsService = /** @class */ (function () {
                function JwtUtilsService() {
                }
                JwtUtilsService.prototype.getRoles = function (token) {
                    var jwtData = token.split('.')[1];
                    var decocdedJwtJsonData = window.atob(jwtData);
                    var decodedJwtData = JSON.parse(decocdedJwtJsonData);
                    return decodedJwtData.roles.map(function (x) { return x.authority; }) || [];
                };
                return JwtUtilsService;
            }());
            JwtUtilsService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()
            ], JwtUtilsService);
            /***/ 
        }),
        /***/ "./src/app/service/security/token-interceptor.service.ts": 
        /*!***************************************************************!*\
          !*** ./src/app/service/security/token-interceptor.service.ts ***!
          \***************************************************************/
        /*! exports provided: TokenInterceptorService */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TokenInterceptorService", function () { return TokenInterceptorService; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _authentication_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./authentication.service */ "./src/app/service/security/authentication.service.ts");
            var TokenInterceptorService = /** @class */ (function () {
                function TokenInterceptorService(inj) {
                    this.inj = inj;
                }
                TokenInterceptorService.prototype.intercept = function (req, next) {
                    var authenticationService = this.inj.get(_authentication_service__WEBPACK_IMPORTED_MODULE_2__["AuthenticationService"]);
                    req = req.clone({
                        setHeaders: {
                            'X-Auth-Token': "" + authenticationService.getToken()
                        }
                    });
                    return next.handle(req);
                };
                return TokenInterceptorService;
            }());
            TokenInterceptorService.ctorParameters = function () { return [
                { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Injector"] }
            ]; };
            TokenInterceptorService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
                    providedIn: 'root'
                })
            ], TokenInterceptorService);
            /***/ 
        }),
        /***/ "./src/environments/environment.ts": 
        /*!*****************************************!*\
          !*** ./src/environments/environment.ts ***!
          \*****************************************/
        /*! exports provided: environment */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function () { return environment; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            // This file can be replaced during build by using the `fileReplacements` array.
            // `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
            // The list of file replacements can be found in `angular.json`.
            var environment = {
                production: false
            };
            /*
             * For easier debugging in development mode, you can import the following file
             * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
             *
             * This import should be commented out in production mode because it will have a negative impact
             * on performance if an error is thrown.
             */
            // import 'zone.js/dist/zone-error';  // Included with Angular CLI.
            /***/ 
        }),
        /***/ "./src/main.ts": 
        /*!*********************!*\
          !*** ./src/main.ts ***!
          \*********************/
        /*! no exports provided */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm2015/platform-browser-dynamic.js");
            /* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
            /* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");
            if (_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].production) {
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["enableProdMode"])();
            }
            Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_3__["AppModule"])
                .catch(function (err) { return console.error(err); });
            /***/ 
        }),
        /***/ 0: 
        /*!***************************!*\
          !*** multi ./src/main.ts ***!
          \***************************/
        /*! no static exports found */
        /***/ (function (module, exports, __webpack_require__) {
            module.exports = __webpack_require__(/*! C:\Users\Jenci\Downloads\Eniko-fajlok3\Eniko-fajlok2\JS\Angular projects\elevators\client\elevators-front-end\src\main.ts */ "./src/main.ts");
            /***/ 
        })
    }, [[0, "runtime", "vendor"]]]);
//# sourceMappingURL=main-es2015.js.map
//# sourceMappingURL=main-es5.js.map
//# sourceMappingURL=main-es5.js.map
package com.liftovi.data;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import com.liftovi.model.Elevator;


@Component
public interface ElevatorRepository extends JpaRepository<Elevator, Long> {

	public List<Elevator> findAllByBuildingIdAndWorkingTrue(Long buildingId);
	
	public Page<Elevator> findAllByBuildingIdAndCode(Long buildingId, int code, Pageable page);
	
	public Page<Elevator> findAllByBuildingIdAndWorking(Long buldingId, boolean working, Pageable page);
	
	public Page<Elevator> findAllByBuildingIdAndCodeAndWorking(Long buildingId, int code, boolean working, Pageable page);
	
	public Page<Elevator> findAllByBuildingId(Long buildingId, Pageable page);
}

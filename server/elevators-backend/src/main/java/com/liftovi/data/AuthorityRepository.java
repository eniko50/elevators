package com.liftovi.data;

import org.springframework.data.jpa.repository.JpaRepository;

import com.liftovi.model.user.SecurityAuthority;

public interface AuthorityRepository extends JpaRepository<SecurityAuthority, Long> {
	
	SecurityAuthority findByName(String name);

}

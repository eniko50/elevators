package com.liftovi.data;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import com.liftovi.model.user.SecurityUserAuthority;

@Component
public interface UserAuthority extends  JpaRepository<SecurityUserAuthority, Long> {

}

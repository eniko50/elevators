package com.liftovi.data;

import org.springframework.data.jpa.repository.JpaRepository;

import com.liftovi.model.Comment;

public interface CommentRepository extends JpaRepository<Comment, Long> {

}

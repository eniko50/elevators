package com.liftovi.data;

import org.springframework.data.jpa.repository.JpaRepository;

import com.liftovi.model.user.SecurityUser;

public interface UserRepository extends JpaRepository<SecurityUser, Long> {
	  public SecurityUser findByUsername(String username);
}

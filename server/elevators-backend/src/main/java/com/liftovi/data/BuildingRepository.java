package com.liftovi.data;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import com.liftovi.model.Building;

@Component
public interface BuildingRepository  extends JpaRepository<Building, Long>{
	
	Page<Building> findByNameContains(String name, Pageable page);
}

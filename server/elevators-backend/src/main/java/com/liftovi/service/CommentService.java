package com.liftovi.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.liftovi.data.CommentRepository;
import com.liftovi.model.Comment;

@Component
public class CommentService {
	
	@Autowired
	CommentRepository commentRepository;

	public <S extends Comment> S save(S entity) {
		return commentRepository.save(entity);
	}

	public Optional<Comment> findById(Long id) {
		return commentRepository.findById(id);
	}

	public List<Comment> findAll() {
		return commentRepository.findAll();
	}
	
	
}

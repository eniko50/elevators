package com.liftovi.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.liftovi.data.BuildingRepository;
import com.liftovi.model.Building;


@Component
public class BuildingService {
	
	@Autowired
	BuildingRepository buildingRepository;
	
	public List<Building> findAll() {
		return buildingRepository.findAll();
	}
	
	@Transactional
	public Building findOne(Long id) {
		return buildingRepository.findById(id).get();
	}
	
	public Building save(Building building) {
		return buildingRepository.save(building);
	}
	
	public void remove(Long id) {
		buildingRepository.deleteById(id);
	}

	public Page<Building> findAll(Pageable pageable) {
		return buildingRepository.findAll(pageable);
	}

	public Page<Building> findByNameContains(String name, Pageable page) {
		return buildingRepository.findByNameContains(name, page);
	}
	
	
}

package com.liftovi.service;

import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.liftovi.data.AuthorityRepository;
import com.liftovi.data.UserAuthority;
import com.liftovi.data.UserRepository;
import com.liftovi.model.user.SecurityAuthority;
import com.liftovi.model.user.SecurityUser;
import com.liftovi.model.user.SecurityUserAuthority;


@Service
public class UserDetailsServiceImpl implements UserDetailsService {

	  @Autowired
	  private UserRepository userRepository;
	  
	  @Autowired
	  AuthorityRepository authorityRepository;
	  
	  @Autowired
	  UserAuthority userAuthority;

	  @Autowired
	  PasswordEncoder passwordEncoder;
	  
	  @Override
	  @Transactional
	  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		SecurityUser user = userRepository.findByUsername(username);
		if (user == null) {
		  throw new UsernameNotFoundException(String.format("No user found with username '%s'.", username));
		} else {
			List<GrantedAuthority> grantedAuthorities = user.getUserAuthorities().stream()
		            .map(authority -> new SimpleGrantedAuthority(authority.getAuthority().getName()))
		            .collect(Collectors.toList());
			
			return new org.springframework.security.core.userdetails.User(
				  user.getUsername(),
				  user.getPassword(),
				  grantedAuthorities);
		}
	  }
	  
	  public SecurityUser register(SecurityUser securityUser, List<String> roles) {
		  if(userRepository.findByUsername(securityUser.getUsername()) != null) {
			  return null;
		  } else {
			  
			  SecurityUserAuthority sua = new SecurityUserAuthority();

			  for(String role : roles) {
				  SecurityAuthority authority = authorityRepository.findByName(role);
				  
				  sua.setAuthority(authority);
				  sua.setUser(securityUser);
				  
				  securityUser.getUserAuthorities().add(sua);
			  }
			  
			  securityUser.setPassword(passwordEncoder.encode(securityUser.getPassword()));
			  userRepository.save(securityUser);
			  userAuthority.save(sua);
			  return securityUser;
		  }
	  }
}

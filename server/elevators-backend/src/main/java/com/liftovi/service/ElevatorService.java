package com.liftovi.service;

import java.util.List;
import java.util.OptionalInt;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import com.liftovi.data.ElevatorRepository;
import com.liftovi.model.Elevator;

@Component
public class ElevatorService {

	@Autowired
	ElevatorRepository elevatorRepository;
	
	public Page<Elevator> findAll(Pageable page) {
		return elevatorRepository.findAll(page);
	}
	
	public Elevator findOne(Long id) {
		return elevatorRepository.findById(id).get();
	}
	
	public Elevator save(Elevator elevator) {
		return elevatorRepository.save(elevator);
	}
	
	public void remove(Long id) {
		elevatorRepository.deleteById(id);
	}
	
	public OptionalInt rideElevator(Long buildingId, int start, int end) {
		List<Elevator> workingElevatorsInBuilding = elevatorRepository.findAllByBuildingIdAndWorkingTrue(buildingId);
		
		if (workingElevatorsInBuilding.isEmpty()) {
			return OptionalInt.empty();
		}
		
		Elevator elevatorUsed = workingElevatorsInBuilding.get(0);
		
		int currentToStartFloor = Math.abs(elevatorUsed.getCurrentFloor() - start); 
		int startToEndFloor = Math.abs(end - start);
		int totalFloorsMoved = currentToStartFloor + startToEndFloor;
		
		elevatorUsed.decreaseFloorsLeft(totalFloorsMoved);
		elevatorUsed.setCurrentFloor(end);
		elevatorRepository.save(elevatorUsed);
		
		checkIfElevatorsShouldBeWorking();
		
		return OptionalInt.of(elevatorUsed.getCode());
	}
	
	public void checkIfElevatorsShouldBeWorking() {
		elevatorRepository.findAll().stream()
			.filter(elevator -> elevator.getFloorsLeft() < 1)
			.forEach(elevator -> {
				elevator.setWorking(false);
				elevatorRepository.save(elevator);
			});
	}

	public Page<Elevator> findAllByBuildingIdAndCode(Long buildingId, int code, Pageable page) {
		return elevatorRepository.findAllByBuildingIdAndCode(buildingId, code, page);
	}

	public Page<Elevator> findAllByBuildingIdAndWorking(Long buldingId, boolean working, Pageable page) {
		return elevatorRepository.findAllByBuildingIdAndWorking(buldingId, working, page);
	}

	public Page<Elevator> findAllByBuildingIdAndCodeAndWorking(Long buildingId, int code, boolean working,
			Pageable page) {
		return elevatorRepository.findAllByBuildingIdAndCodeAndWorking(buildingId, code, working, page);
	}

	public Page<Elevator> findAllByBuildingId(Long buildingId, Pageable page) {
		return elevatorRepository.findAllByBuildingId(buildingId, page);
	}
}

package com.liftovi.web.dto;

public class TokenDTO {
	
	private String token;

	public TokenDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public TokenDTO(String token) {
		super();
		this.token = token;
	}
	
	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
}

package com.liftovi.web.dto;

import com.liftovi.model.Comment;

public class CommentDto {
	
	private Long id;
	private String nickname;
	private String content;
	private int likes;
	private int dislikes;
	
	
	public CommentDto() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public CommentDto(Comment comment) {
		this.id = comment.getId();
		this.nickname = comment.getNickname();
		this.content = comment.getContent();
		this.likes = comment.getLikes();
		this.dislikes = comment.getDislikes();
	}
	public CommentDto(String nickname, String content, int likes, int dislikes) {
		super();
		this.nickname = nickname;
		this.content = content;
		this.likes = likes;
		this.dislikes = dislikes;
	}


	public String getNickname() {
		return nickname;
	}
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public int getLikes() {
		return likes;
	}
	public void setLikes(int likes) {
		this.likes = likes;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getDislikes() {
		return dislikes;
	}

	public void setDislikes(int dislikes) {
		this.dislikes = dislikes;
	}
}

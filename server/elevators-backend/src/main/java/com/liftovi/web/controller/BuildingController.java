package com.liftovi.web.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.liftovi.model.Building;
import com.liftovi.model.Comment;
import com.liftovi.service.BuildingService;
import com.liftovi.service.CommentService;
import com.liftovi.web.dto.BuildingDto;
import com.liftovi.web.dto.CommentDto;

@RestController
public class BuildingController {
	
	@Autowired
	private BuildingService buildingService;
	
	@Autowired
	private CommentService commentService;
	
	@GetMapping("api/buildings")
	public ResponseEntity<List<BuildingDto>> getAllBuildingsPage(
			@RequestParam(defaultValue = "", name="name") String name,
			Pageable page){
		List<BuildingDto> buildings = buildingService.findByNameContains(name, page)
				.stream().map(BuildingDto :: new)
				.collect(Collectors.toList());
		
		return new ResponseEntity<List<BuildingDto>>(buildings, HttpStatus.OK);
	}
	
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@PutMapping("api/buildings/{id}")
	public ResponseEntity<BuildingDto> editBuilding( @PathVariable Long id,
			@RequestBody BuildingDto buildingDto){
		
		Building building = buildingService.findOne(id);
		building.setId(id);
		building.setName(buildingDto.getName());
		building.setFloors(buildingDto.getFloors());
		
		Building savedBuilding = buildingService.save(building);
		
		return new ResponseEntity<BuildingDto>(new BuildingDto(savedBuilding), HttpStatus.OK);
	}
	
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@DeleteMapping("api/buildings/{id}")
	public ResponseEntity<Void> delete(@PathVariable Long id) {
		Building building = buildingService.findOne(id);
		if(building == null) {
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		}
		buildingService.remove(id);
		return new ResponseEntity<Void>(HttpStatus.OK);
	}
	
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@PostMapping("api/buildings")
	public ResponseEntity<BuildingDto> add(@RequestBody BuildingDto buildingDto) {
		Building building = new Building();
		building.setName(buildingDto.getName());
		building.setFloors(buildingDto.getFloors());
		
		Building savedBuilding = buildingService.save(building);
		return new ResponseEntity<BuildingDto>(new BuildingDto(savedBuilding), HttpStatus.CREATED);
	}
	
	@GetMapping("api/buildings/comments")
	public ResponseEntity<List<CommentDto>> getComments(){
		
		List<CommentDto> commentDtos = commentService.findAll().stream()
				.map(CommentDto::new).collect(Collectors.toList());
		
		return new ResponseEntity<List<CommentDto>>(commentDtos, HttpStatus.OK);
	}
	
	@PostMapping("api/buildings/comments")
	public ResponseEntity<CommentDto> addComment(@RequestBody CommentDto commentDto){
		

		Comment comment = new Comment();
		
		comment.setNickname(commentDto.getNickname());
		comment.setContent(commentDto.getContent());
		
		Comment savedComment = commentService.save(comment);
		
		return new ResponseEntity<CommentDto>(new CommentDto(savedComment), HttpStatus.CREATED);
	}
	
	@PutMapping("api/buildings/comments/{id}/like")
	public ResponseEntity<CommentDto> likeComment(@PathVariable Long id) {
		
		Comment comment = commentService.findById(id).get();
		
		comment.increaseLikes();
		Comment savedComment = commentService.save(comment);
		
		return new ResponseEntity<CommentDto>(new CommentDto(savedComment), HttpStatus.OK);
	}
	
	
	@PutMapping("api/buildings/comments/{id}/dislike")
	public ResponseEntity<CommentDto> dislikeComment(@PathVariable Long id) {
		
		Comment comment = commentService.findById(id).get();
		
		comment.increaseDislikes();
		Comment savedComment = commentService.save(comment);
		
		return new ResponseEntity<CommentDto>(new CommentDto(savedComment), HttpStatus.OK);
	}
	
}

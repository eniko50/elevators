package com.liftovi.web.dto;

public class ElevatorRideDto {

	private Long buildingId;
	private int startFloor;
	private int endFloor;
	
	public Long getBuildingId() {
		return buildingId;
	}
	
	public void setBuildingId(Long buildingId) {
		this.buildingId = buildingId;
	}
	
	public int getStartFloor() {
		return startFloor;
	}
	
	public void setStartFloor(int startFloor) {
		this.startFloor = startFloor;
	}
	
	public int getEndFloor() {
		return endFloor;
	}
	
	public void setEndFloor(int endFloor) {
		this.endFloor = endFloor;
	}	
}

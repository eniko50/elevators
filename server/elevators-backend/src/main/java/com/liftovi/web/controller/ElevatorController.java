package com.liftovi.web.controller;

import java.util.List;
import java.util.OptionalInt;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.liftovi.model.Building;
import com.liftovi.model.Elevator;
import com.liftovi.service.BuildingService;
import com.liftovi.service.ElevatorService;
import com.liftovi.web.dto.ElevatorDto;
import com.liftovi.web.dto.ElevatorRideDto;

@RestController
public class ElevatorController {

	@Autowired
	private BuildingService buildingService;

	@Autowired
	private ElevatorService elevatorService;

	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@GetMapping("/api/elevators")
	public ResponseEntity<List<ElevatorDto>> getAllElevators(Pageable page) {
		List<ElevatorDto> elevators = elevatorService.findAll(page).stream().map(ElevatorDto::new)
				.collect(Collectors.toList());

		return new ResponseEntity<>(elevators, HttpStatus.OK);
	}

	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@GetMapping("api/buildings/{id}/elevators")
	public ResponseEntity<List<ElevatorDto>> getElevatorsInBuilding(@PathVariable Long id,
			@RequestParam(value = "code", required = false) Integer code,
			@RequestParam(value = "working", required = false) Boolean working, Pageable page) {

		List<ElevatorDto> elevatorDtos;

		if (working == null && code != null) {
			elevatorDtos = elevatorService.findAllByBuildingIdAndCode(id, code, page).stream().map(ElevatorDto::new)
					.collect(Collectors.toList());
			
		} else if(working != null && code == null) {
			elevatorDtos = elevatorService.findAllByBuildingIdAndWorking(id, working, page)
					.stream().map(ElevatorDto::new).collect(Collectors.toList());

			
		} else if(code != null && working != null) {
			elevatorDtos = elevatorService.findAllByBuildingIdAndCodeAndWorking(id, code, working, page)
					.stream().map(ElevatorDto::new).collect(Collectors.toList());

		} else {
			elevatorDtos = elevatorService.findAllByBuildingId(id, page).stream()
					.map(ElevatorDto::new).collect(Collectors.toList());
		}
		return new ResponseEntity<List<ElevatorDto>>(elevatorDtos, HttpStatus.OK);
		
	}

	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@PutMapping("/api/elevators/{id}")
	public ResponseEntity<ElevatorDto> editElevator(@PathVariable Long id, @RequestBody ElevatorDto elevatorDto) {
		Elevator elevator = elevatorService.findOne(id);
		elevator.setId(id);
		elevator.setCode(elevatorDto.getCode());
		elevator.setCurrentFloor(elevatorDto.getCurrentFloor());
		elevator.setWorking(elevatorDto.isWorking());
		elevator.setFloorsLeft(elevatorDto.getFloorsLeft());

		Elevator savedElevator = elevatorService.save(elevator);
		return new ResponseEntity<ElevatorDto>(new ElevatorDto(savedElevator), HttpStatus.OK);
	}

	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@PostMapping("api/buildings/{id}/elevators")
	public ResponseEntity<ElevatorDto> postElevator(@RequestBody ElevatorDto elevatorDto, @PathVariable Long id) {
		Building building = buildingService.findOne(id);
		Elevator elevator = new Elevator();
		elevator.setCode(elevatorDto.getCode());
		elevator.setCurrentFloor(elevatorDto.getCurrentFloor());
		elevator.setFloorsLeft(elevatorDto.getFloorsLeft());
		elevator.setWorking(elevatorDto.isWorking());
		elevator.setBuilding(building);

		Elevator savedElevator = elevatorService.save(elevator);

		return new ResponseEntity<ElevatorDto>(new ElevatorDto(savedElevator), HttpStatus.CREATED);
	}

	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@DeleteMapping("api/elevators/{id}")
	public ResponseEntity deleteElevator(@PathVariable Long id) {
		Elevator elevator = elevatorService.findOne(id);
		if (elevator == null) {
			return new ResponseEntity(HttpStatus.NOT_FOUND);
		}
		elevatorService.remove(id);
		return new ResponseEntity(HttpStatus.OK);
	}

	@PreAuthorize("isAuthenticated()")
	@PostMapping("/api/rideElevator")
	public ResponseEntity<Integer> rideElevator(@RequestBody ElevatorRideDto elevatorRideDto, Pageable page) {
		OptionalInt result = elevatorService.rideElevator(elevatorRideDto.getBuildingId(),
				elevatorRideDto.getStartFloor(), elevatorRideDto.getEndFloor());

		if (result.isPresent()) {
			return new ResponseEntity<Integer>(result.getAsInt(), HttpStatus.OK);
		} else {
			return new ResponseEntity<Integer>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}

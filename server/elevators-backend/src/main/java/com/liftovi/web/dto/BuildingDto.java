package com.liftovi.web.dto;

import java.util.List;
import java.util.stream.Collectors;

import com.liftovi.model.Building;

public class BuildingDto {

	private Long id;
	private String name;
	private int floors;
	private List<ElevatorDto> elevators;
	
	public BuildingDto() {

	}

	public BuildingDto(Building building) {
		this.id = building.getId();
		this.name = building.getName();
		this.floors = building.getFloors();
		this.elevators = building.getElevators().stream().map(ElevatorDto::new).collect(Collectors.toList());
	}

	public String getName() {
		return name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getFloors() {
		return floors;
	}

	public void setFloors(int floors) {
		this.floors = floors;
	}

	public List<ElevatorDto> getElevators() {
		return elevators;
	}

	public void setElevators(List<ElevatorDto> elevators) {
		this.elevators = elevators;
	}
}

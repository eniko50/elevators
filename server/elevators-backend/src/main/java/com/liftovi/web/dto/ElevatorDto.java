package com.liftovi.web.dto;

import com.liftovi.model.Elevator;

public class ElevatorDto {

	private Long id;

	private int code;

	private int currentFloor;

	private int floorsLeft;
	
	private String building;
	
	private boolean working;
	
	public ElevatorDto() {

	}

	public ElevatorDto(Elevator elevator) {
		this.id = elevator.getId();
		this.code = elevator.getCode();
		this.currentFloor = elevator.getCurrentFloor();
		this.floorsLeft = elevator.getFloorsLeft();
		this.working = elevator.isWorking();
		this.building = elevator.getBuilding().getName();
	}


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}


	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public int getCurrentFloor() {
		return currentFloor;
	}

	public void setCurrentFloor(int currentFloor) {
		this.currentFloor = currentFloor;
	}

	public int getFloorsLeft() {
		return floorsLeft;
	}

	public void setFloorsLeft(int floorsLeft) {
		this.floorsLeft = floorsLeft;
	}

	public boolean isWorking() {
		return working;
	}

	public void setWorking(boolean working) {
		this.working = working;
	}

	public String getBuilding() {
		return building;
	}

	public void setBuilding(String building) {
		this.building = building;
	}
}

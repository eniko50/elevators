package com.liftovi.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;


@Entity
public class Comment {
	
	@Id
	@GeneratedValue
	private Long id;
	private String nickname;
	private String content;
	private int likes = 0;
	private int dislikes = 0;
	
	public Comment(Long id, String nickname, String content, int likes, int dislikes) {
		super();
		this.id = id;
		this.nickname = nickname;
		this.content = content;
		this.likes = likes;
		this.dislikes = dislikes;
	}
	
	public void increaseLikes() {
		this.likes += 1;
	}
	
	public void decreaseLikes() {
		this.likes -= 1;
	}
	
	public void increaseDislikes() {
		this.dislikes +=1;
	}
	
	public void decreaseDislikes() {
		this.dislikes -= 1;
	}

	public Comment() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public int getLikes() {
		return likes;
	}

	public void setLikes(int likes) {
		this.likes = likes;
	}

	public int getDislikes() {
		return dislikes;
	}

	public void setDislikes(int dislikes) {
		this.dislikes = dislikes;
	}
}

package com.liftovi.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Elevator {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;

	private int code;
	
	private int currentFloor;
	
	private int floorsLeft;
	
	private boolean working;
	
	@JsonIgnore
	@ManyToOne(fetch = FetchType.EAGER) 
	Building building;


	public Elevator(Long id, int code, int currentFloor, int floorsLeft, boolean working, Building building) {
		super();
		this.id = id;
		this.code = code;
		this.currentFloor = currentFloor;
		this.floorsLeft = floorsLeft;
		this.working = working;
		this.building = building;
	}


	public Elevator() {
		super();
	}

	// funkcija za smanjenje broja spratova koji moze da predje
	public void decreaseFloorsLeft(int number) {
		int remainingFloors = this.floorsLeft - number;
		// broj ne moze da ode ispod nule
		this.floorsLeft = remainingFloors < 0 ? 0 : remainingFloors;
	}

	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public int getCode() {
		return code;
	}


	public void setCode(int code) {
		this.code = code;
	}


	public int getCurrentFloor() {
		return currentFloor;
	}
	
	public int getFloorsLeft() {
		return floorsLeft;
	}


	public void setFloorsLeft(int floorsLeft) {
		this.floorsLeft = floorsLeft;
	}


	public boolean isWorking() {
		return working;
	}


	public void setWorking(boolean working) {
		this.working = working;
	}


	public void setCurrentFloor(int currentFloor) {
		this.currentFloor = currentFloor;
	}


	public Building getBuilding() {
		return building;
	}


	public void setBuilding(Building building) {
		this.building = building;
	}

//	@Override
//	public int hashCode() {
//		final int prime = 31;
//		int result = 1;
//		result = prime * result + ((building == null) ? 0 : building.hashCode());
//		result = prime * result + code;
//		result = prime * result + currentFloor;
//		result = prime * result + floorsLeft;
//		result = prime * result + ((id == null) ? 0 : id.hashCode());
//		result = prime * result + (working ? 1231 : 1237);
//		return result;
//	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Elevator other = (Elevator) obj;
		if (building == null) {
			if (other.building != null)
				return false;
		} else if (!building.equals(other.building))
			return false;
		if (code != other.code)
			return false;
		if (currentFloor != other.currentFloor)
			return false;
		if (floorsLeft != other.floorsLeft)
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (working != other.working)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Elevator [id=" + id + ", code=" + code + ", currentFloor=" + currentFloor + ", floorsLeft=" + floorsLeft
				+ ", working=" + working + ", building=" + building + "]";
	}
}

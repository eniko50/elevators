package com.liftovi.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
public class Building {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String name;

	private int floors;
		
	@JsonManagedReference
	@OneToMany(mappedBy = "building", fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
	private Set<Elevator> elevators = new HashSet<>();

	public Building(Long id, String name, int floors, Set<Elevator> elevators) {
		super();
		this.id = id;
		this.name = name;
		this.floors = floors;
		this.elevators = elevators;
	}

	public Building() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getFloors() {
		return floors;
	}

	public void setFloors(int floor) {
		this.floors = floor;
	}
	
	public Set<Elevator> getElevators() {
		return elevators;
	}

	public void setElevators(Set<Elevator> elevators) {
		this.elevators = elevators;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Building other = (Building) obj;
		if (elevators == null) {
			if (other.elevators != null)
				return false;
		} else if (!elevators.equals(other.elevators))
			return false;
		if (floors != other.floors)
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Building [id=" + id + ", name=" + name + ", floors=" + floors + ", elevators=" + elevators + "]";
	}

}

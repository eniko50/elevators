import {
  Component,
  OnInit,
  Output,
  EventEmitter
} from '@angular/core';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css'],
})
export class SearchComponent implements OnInit {

  @Output()
  setSearchTerm: EventEmitter<string> = new EventEmitter();

  searchTerm: string;

  constructor() {
    this.searchTerm = "";
   }

  ngOnInit() {
  }

  search() {
    this.setSearchTerm.emit(this.searchTerm);
  }

  reset() {
    this.searchTerm = "";
    this.setSearchTerm.emit(this.searchTerm);
  }

}

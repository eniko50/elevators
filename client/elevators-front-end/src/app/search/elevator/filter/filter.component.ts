import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.css']
})
export class FilterComponent implements OnInit {

  @Output()
  setCode: EventEmitter<number> = new EventEmitter();

  @Output()
  setWorking: EventEmitter<boolean> = new EventEmitter();

  working: boolean;
  code: number;

  constructor() {
    this.code = undefined;
    this.working = undefined;
   }

  ngOnInit() {
  }

  filter() {
    this.setCode.emit(this.code);
    this.setWorking.emit(this.working);
  }

  reset() {
    this.code = undefined;
    this.working = undefined;
    this.setCode.emit(this.code);
    this.setWorking.emit(this.working);
  }

}

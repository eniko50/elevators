import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { Elevator } from 'src/app/models/elevator.model';
import { ElevatorService } from 'src/app/service/elevator.service';
import { ActivatedRoute } from '@angular/router';
import { AuthenticationService } from 'src/app/service/security/authentication.service';

@Component({
  selector: 'app-elevator-page',
  templateUrl: './elevator-page.component.html',
  styleUrls: ['./elevator-page.component.css']
})
export class ElevatorPageComponent implements OnInit {

  public activeElevator: Elevator;
  public elevators: Elevator[];
  public page = 0;
  public working: boolean;
  public code: number;

  public isAdministrator: boolean;

  private id: number;

  constructor(private elevatorService: ElevatorService,
    private route: ActivatedRoute,
    private authenticationService: AuthenticationService,
  ) { }

  ngOnInit() {
    //bitno da ne dobijamo gresku undefined pri ucitavanju
    this.isAdministrator = this.authenticationService.getCurrentUser() ?
      this.authenticationService.getCurrentUser().roles.indexOf('ROLE_ADMIN') !== -1 : false;
    this.resetActiveElevator();
    this.loadData();
  }

  loadData() {
    this.route.params.subscribe(param => {
      this.id = param.id;
      this.elevatorService.getInBuildingAndCodeOrWorking(this.id,
        this.code, this.working, this.page)
        .subscribe((res: any) => {
          this.elevators = res;
        });
    });
  }

  addElevator(elevator: Elevator) {
    this.route.params.subscribe(param => {
      this.id = param.id;
      this.elevatorService.addElevator(elevator, this.id)
        .subscribe((res: any) => {
          this.loadData();
        });
    })

  }
  editElevator(elevator: Elevator) {
    this.elevatorService.editElevator(elevator).subscribe((res: Elevator) => {
      this.loadData();
      this.resetActiveElevator();
    })
  }

  deleteElevator(id: number) {
    this.elevatorService.deleteElevator(id).subscribe(
      (res: any) => {
        this.loadData();
      }
    )
  }

  resetActiveElevator() {
    this.activeElevator = new Elevator({
      id: 0,
      building: null,
      code: 0,
      currentFloor: 0,
      floorsLeft: 0,
      working: false
    })
  }

  setActiveElevator(elevator: Elevator) {
    this.activeElevator = new Elevator(elevator);
  }

  setCode(code: number) {
    this.code = code;
    this.loadData();
  }

  setWorking(working: boolean) {
    this.working = working;
    this.loadData();
  }

  resetFilter() {
    this.code = undefined;
    this.working = undefined;
    this.loadData();
  }

  nextPage() {
    this.page += 1;
    this.loadData();
  }

  prevPage() {
    if (this.page > 0) {
      this.page -= 1;
      this.loadData();
    }
  }

  callPage(pageNumber: number) {
    this.page = pageNumber;
    this.loadData();
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ElevatorPageComponent } from './elevator-page.component';

describe('ElevatorPageComponent', () => {
  let component: ElevatorPageComponent;
  let fixture: ComponentFixture<ElevatorPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ElevatorPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ElevatorPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

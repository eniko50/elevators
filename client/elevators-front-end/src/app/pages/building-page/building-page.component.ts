import { Component, OnInit, Input } from '@angular/core';
import { Building } from 'src/app/models/building.model';
import { BuildingsService } from 'src/app/service/buildings.service';
import { ElevatorService } from 'src/app/service/elevator.service';
import { RideElevator } from 'src/app/models/rideElevator.model';
import { AuthenticationService } from 'src/app/service/security/authentication.service';
import { Comment } from 'src/app/models/comment.model';

@Component({
  selector: 'app-building-page',
  templateUrl: './building-page.component.html',
  styleUrls: ['./building-page.component.css']
})
export class BuildingPageComponent implements OnInit {

  public buildings: Building[];
  public activeBuilding: Building;
  public activeBuildingId: number;
  public comments: Comment[];

  public isAdministrator: boolean;
  public isLoggedIn: boolean;
  public page = 0;
  public buildingName: string = "";

  constructor(private buildingsService: BuildingsService,
    private authenticationService: AuthenticationService,
    private elevatorService: ElevatorService) { }

  ngOnInit() {
    this.isAdministrator = this.authenticationService.getCurrentUser() ?
      this.authenticationService.getCurrentUser().roles.indexOf('ROLE_ADMIN') !== -1 : false;
    this.isLoggedIn = this.authenticationService.isLoggedIn();
    this.resetActiveBuilding();
    this.loadData();
  }

  deleteBuilding(id: number) {
    this.buildingsService.deleteBuilding(id).subscribe(
      (res: any) => {
        this.loadData();
      }
    )
  }

  editBuilding(buiding: Building) {
    this.buildingsService.editBuilding(buiding).subscribe(
      (res: any) => {
        this.loadData();
        this.resetActiveBuilding();
      }
    )
  }

  loadData() {
    this.buildingsService.getBuildings(this.page, this.buildingName).subscribe(
      (res: any) => {
        this.buildings = res;
        this.loadComments();
      }
    );
  }

  loadComments() {
    this.buildingsService.getComments().subscribe(
      (res: any) => {
        this.comments = res;
      }
    );
  }

  likeComment(comment: Comment) {
    this.buildingsService.likeComment(comment)
      .subscribe((res:any) => {
        this.loadComments();
      });
  }

  dislikeComment(comment: Comment) {
    this.buildingsService.dislikeComment(comment)
    .subscribe((res: any) => {
      this.loadComments();
    });
  }

  postComment(comment: Comment) {
    this.buildingsService.postComment(comment).subscribe(
      (res: any) => {
        this.loadComments();
      }
    )
  }

  resetCommentForm() {
    
  }

  setActiveBuilding(building: Building) {
    this.activeBuilding = new Building(building);
  }

  setBuildingIdForElevatorRiding(id: number) {
    this.activeBuildingId = id;
  }

  resetActiveBuilding() {
    this.activeBuilding = new Building({
      name: '',
      floors: 0,
      elevators: null
    });
  }

  rideElevator(rideElevator: RideElevator) {
    this.elevatorService.rideElevator(rideElevator)
      .subscribe((res: any) => {
      });
  }

  search(searchTerm: string) {
    this.buildingsService.getBuildings(this.page, searchTerm)
      .subscribe((buidings: Building[]) => {
        this.buildings = buidings;
      })
  }

  nextPage() {
    this.page += 1;
    this.loadData();
  }

  prevPage() {
    if (this.page > 0) {
      this.page -= 1;
      this.loadData();
    }
  }

  callPage(pageNumber: number) {
    this.page = pageNumber;
    this.loadData();
  }
}

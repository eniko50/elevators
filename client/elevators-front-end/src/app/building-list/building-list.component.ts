import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import { Building } from '../models/building.model';
import { RideElevator } from '../models/rideElevator.model';

@Component({
  selector: 'app-building-list',
  templateUrl: './building-list.component.html',
  styleUrls: ['./building-list.component.css']
})
export class BuildingListComponent implements OnInit {

  @Input()
  public isAdministrator: boolean;

  @Input()
  isLoggedIn;
  
  @Input()
  buildings: Building[];

  @Output()
  markBuildingForEditing: EventEmitter<Building> = new EventEmitter();

  @Output()
  buildingToDelete: EventEmitter<number> = new EventEmitter();

  @Output()
  markBuildingIdForElevatorForRiding: EventEmitter<number> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  editBuilding(building: Building) {
    this.markBuildingForEditing.emit(building);
  }

  deleteBuilding(id: number) {
    this.buildingToDelete.emit(id);
  }

  rideElevator(id: number) {
    this.markBuildingIdForElevatorForRiding.emit(id);
  }
}

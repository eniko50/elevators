import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Comment } from '../models/comment.model';
import { Building } from '../models/building.model';
import { BuildingsService } from '../service/buildings.service';

@Component({
  selector: 'app-comment-list',
  templateUrl: './comment-list.component.html',
  styleUrls: ['./comment-list.component.css']
})
export class CommentListComponent implements OnInit {

  @Input()
  comments: Comment[];

  @Output()
  commentLiked: EventEmitter<Comment> = new EventEmitter();

  @Output()
  commentDisliked: EventEmitter<Comment> = new EventEmitter();

  @Output()
  commentPosted: EventEmitter<Comment> = new EventEmitter();

  newComment: Comment;

  constructor() { }

  ngOnInit() {
    this.resetComment();
  }

  likeComment(comment: Comment) {
    this.commentLiked.emit(comment);
  }

  dislikeComment(comment: Comment) {
    this.commentDisliked.emit(comment);
  }

  postComment(){
    this.commentPosted.emit(this.newComment);
    this.resetComment();
  }

  resetComment() {
    this.newComment = new Comment({
      nickname: '',
      content: ''
    })
  }
}

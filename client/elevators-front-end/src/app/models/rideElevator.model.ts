export class RideElevator {
    buildingId: number;
    startFloor: number;
    endFloor: number;

    constructor(rideElevatorCfg: RideElevatorInterface) {
        this.buildingId = rideElevatorCfg.buildingId;
        this.startFloor = rideElevatorCfg.startFloor;
        this.endFloor = rideElevatorCfg.endFloor;
    }
}

export interface RideElevatorInterface {
    buildingId: number;
    startFloor: number;
    endFloor: number;
}
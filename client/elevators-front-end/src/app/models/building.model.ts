import { ElevatorInterface, Elevator } from './elevator.model';
import { Comment } from './comment.model';

export interface BuildingInterface {
    id?: number,
    name: string,
    floors: number,
    elevators?: Elevator[],
}

export class Building implements BuildingInterface {
    public id: number;
    public name: string;
    public floors: number;
    public elevators: Elevator[];

    constructor(buildingCfg: BuildingInterface) {
        this.id = buildingCfg.id;
        this.name = buildingCfg.name;
        this.floors = buildingCfg.floors;
        this.elevators = buildingCfg.elevators;
    }
}

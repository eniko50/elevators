import { Building } from './building.model';

export interface ElevatorInterface {
    id?: number,
    code: number,
    currentFloor: number,
    floorsLeft: number,
    working: boolean,
    building: Building
}

export class Elevator implements ElevatorInterface{
    public id: number;
    public code: number;
    public currentFloor: number;
    public floorsLeft: number;
    public working: boolean;
    public building: Building;

    constructor(elevatorCfg: ElevatorInterface){
        this.id = elevatorCfg.id;
        this.code = elevatorCfg.code;
        this.currentFloor = elevatorCfg.currentFloor;
        this.floorsLeft = elevatorCfg.floorsLeft;
        this.working = elevatorCfg.working;
        this.building = elevatorCfg.building;
    }

    public decreeseFloorsLeft(number: number) : void {
        let remainingFloors = this.floorsLeft - number;
        this.floorsLeft = remainingFloors < 0 ? 0 : remainingFloors;
    }
}
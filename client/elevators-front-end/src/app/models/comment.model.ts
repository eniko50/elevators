import { Building } from './building.model';

export interface CommentInterface {
    id?: number;
    nickname?: string;
    content?: string;
    likes?: number;
    dislikes?: number;
}

export class Comment implements CommentInterface {
    public id?: number;
    public nickname: string;
    public content: string;
    public likes: number;
    public dislikes: number;

    constructor(commentCfg: CommentInterface) {
        this.id = commentCfg.id;
        this.nickname = commentCfg.nickname;
        this.content = commentCfg.content;
        this.likes = commentCfg.likes;
        this.dislikes = commentCfg.dislikes;
    }
}
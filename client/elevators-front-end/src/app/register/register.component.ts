import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../service/security/authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  public user;

  public usernameExists: boolean;

  constructor(private authS: AuthenticationService,
    private router: Router) {
    this.user = {};
    this.usernameExists = false;
  }

  ngOnInit() {
  }

  register(): void {
    this.authS.register(this.user.firstName, this.user.lastName, this.user.username, this.user.password)
      .subscribe((isRegistered: boolean) => {
        if (isRegistered) {
          this.router.navigate(["/login"]);
        }
      })
  }

}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BuildingPageComponent } from './pages/building-page/building-page.component';
import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component';
import { ElevatorPageComponent } from './pages/elevator-page/elevator-page.component';
import { CanActivateAuthGuardGuard } from './service/security/can-activate-auth-guard.guard';
import { LoginComponent } from './login/login.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { AboutPageComponent } from './pages/about-page/about-page.component';
import { AddBuildingComponent } from './forms/add-building/add-building.component';
import { AddElevatorComponent } from './forms/add-elevator/add-elevator.component';
import { RegisterComponent } from './register/register.component';


const routes: Routes = [
  {path: 'home', component: HomePageComponent},
  {path: 'about', component: AboutPageComponent},
  {path: 'buildings', component: BuildingPageComponent },
  {path: 'buildings/add', component: AddBuildingComponent, canActivate: [CanActivateAuthGuardGuard]},
  {path: 'buildings/:id/elevators/add', component: AddElevatorComponent, canActivate: [CanActivateAuthGuardGuard]},
  {path: 'buildings/:id/elevators', component: ElevatorPageComponent, canActivate: [CanActivateAuthGuardGuard]},
  {path: 'register', component : RegisterComponent},
  {path: 'login', component: LoginComponent},
  {path: '', redirectTo: 'home', pathMatch: 'full'},
  {path: '**', component: PageNotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

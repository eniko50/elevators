import { TestBed, async, inject } from '@angular/core/testing';

import { CanActivateAuthGuardGuard } from './can-activate-auth-guard.guard';

describe('CanActivateAuthGuardGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CanActivateAuthGuardGuard]
    });
  });

  it('should ...', inject([CanActivateAuthGuardGuard], (guard: CanActivateAuthGuardGuard) => {
    expect(guard).toBeTruthy();
  }));
});

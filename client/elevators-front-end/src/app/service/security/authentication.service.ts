import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { JwtUtilsService } from './jwt-utils.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class AuthenticationService {

  public isAdministrator: boolean;

  private readonly loginPath = '/api/login';
  private readonly registerPath = '/api/register';

  constructor(private http: HttpClient, private jwtUtilsService: JwtUtilsService) { }

  login(username: string, password: string): Observable<boolean> {
    let headers: HttpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' });

    return this.http.post(this.loginPath, JSON.stringify({ username, password }), { headers })
      .pipe(
        map((res: any) => {
          let token = res && res['token'];
          if (token) {
            localStorage.setItem('currentUser', JSON.stringify({
                                  username: username,
                                  roles: this.jwtUtilsService.getRoles(token),
                                  token: token
                                }));
            return true;
          } else {
            return false;
          }
        })
      );
  }

  register(firstName: string, lastName: string, username: string, password: string) : Observable<boolean> {
    let headers: HttpHeaders = new HttpHeaders({'Content-Type' : 'application/json'});

    return this.http.post(this.registerPath, JSON.stringify({firstName, lastName, username, password}), {headers})
    .pipe(
      map((res:any) =>{
        if(res){
          return true;
        } else {
          return false;
        }
      })
    );
  }

  getToken() : string {
    let currentUser = JSON.parse(localStorage.getItem('currentUser'));
    let token = currentUser && currentUser.token;
    return token ? token : "";
  }

  logOut() : void {
    localStorage.removeItem('currentUser');
  }

  isLoggedIn() : boolean {
    if(this.getToken() !== '') {
       return true;
    } else {
      return false;
    }
  }

  getCurrentUser(){
    if(localStorage.currentUser) {
      return JSON.parse(localStorage.currentUser);
    } else {
      return undefined;
    }
  }

  isAdmin() : boolean { 
    return this.getCurrentUser() ?
    this.getCurrentUser().roles.indexOf('ROLE_ADMIN') !== -1 : false;
  }
}

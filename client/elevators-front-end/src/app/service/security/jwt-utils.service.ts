import { Injectable } from '@angular/core';

@Injectable()
export class JwtUtilsService {

  constructor() { }

  getRoles(token: string) {
    let jwtData = token.split('.')[1];
    let decocdedJwtJsonData = window.atob(jwtData);
    let decodedJwtData = JSON.parse(decocdedJwtJsonData);

    return decodedJwtData.roles.map(x => x.authority) || [];
  }
}

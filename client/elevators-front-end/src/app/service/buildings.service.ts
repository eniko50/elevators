import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Building } from '../models/building.model';
import { Comment } from '../models/comment.model';

@Injectable({
  providedIn: 'root'
})
export class BuildingsService {

  private readonly path = 'api/buildings';
  private pageSize = 5;

  constructor(private http: HttpClient) {
   }

  getBuildings(page:number, name: string): Observable<Building[]> {
    return this.http.get<Building[]>(this.path + `?page=${page}&size=${this.pageSize}&name=${name}`);
  }

  deleteBuilding(id: number): Observable<any>{
    return this.http.delete<any>(`${this.path}/${id}`)
  }

  editBuilding(building: Building): Observable<any> {
    return this.http.put(`${this.path}/${building.id}`, building);
  }

  addBuilding(building: Building) : Observable<Building> {
    return this.http.post<Building>(`${this.path}`, building);
  }

  getComments() : Observable<Comment[]> {
    return this.http.get<Comment[]>(`api/buildings/comments`);
  }

  postComment(comment: Comment) : Observable<Comment> {
    return this.http.post<Comment>(`api/buildings/comments`, comment);
  }

  likeComment(comment: Comment) : Observable<any> {
    return this.http.put<any>(`api/buildings/comments/${comment.id}/like`,comment);
  }

  dislikeComment(comment: Comment) : Observable<any> {
    return this.http.put<any>(`api/buildings/comments/${comment.id}/dislike`, comment);
  }
}

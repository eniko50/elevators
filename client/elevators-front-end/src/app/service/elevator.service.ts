import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Elevator } from '../models/elevator.model';
import { Observable } from 'rxjs';
import { RideElevator } from '../models/rideElevator.model';

@Injectable({
  providedIn: 'root'
})
export class ElevatorService {

  private readonly path = 'api/elevators';
  private pageSize = 5;

  constructor(private http : HttpClient) { }

  getElevators() : Observable<Elevator[]>{
    return this.http.get<Elevator[]>(this.path);
  }

  getElevatorsInBuilding(id: number): Observable<Elevator[]> {
    return this.http.get<Elevator[]>(`api/buildings/${id}/elevators`);
  }

  getInBuildingAndCodeOrWorking(id: number, code: number, working: boolean, page: number) : Observable<Elevator[]> {
    let params = new HttpParams();

    if(code !== undefined) {
      params = params.set('code', code.toString());
    }

    if(working!== undefined) {
      params = params.set('working', working.toString());
    }
    return this.http.get<Elevator[]>(`api/buildings/${id}/elevators?page=${page}&size=${this.pageSize}`, {params});
  }
  editElevator(elevator : Elevator) : Observable<Elevator>{
    return this.http.put<Elevator>(`api/elevators/${elevator.id}`, elevator);
  }

  deleteElevator(id: number ) : Observable<any>{
    return this.http.delete<any>(`${this.path}/${id}`);
  }

  addElevator(elevator: Elevator, id: number): Observable<Elevator> {
    return this.http.post<Elevator>(`api/buildings/${id}/elevators`, elevator );
  }

  rideElevator(rideElevator: RideElevator) {
    return this.http.post(`api/rideElevator`, rideElevator);
  }
}

import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { AuthenticationService } from '../service/security/authentication.service';

import { Router } from '@angular/router';
import { Location } from '@angular/common';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class LoginComponent implements OnInit {

  public user;

  public wrongUsernameOrPass: boolean;

  constructor(private authenticationService: AuthenticationService,
    private router: Router,
    private location: Location) {
    this.user = {};
    this.wrongUsernameOrPass = false;
  }

  ngOnInit() {
  }

  login(): void {
    this.authenticationService.login(this.user.username, this.user.password).subscribe(
      (loggedIn: boolean) => {
        if (loggedIn) {
          this.router.navigate(["/home"]);
        } 
      });
  }

}

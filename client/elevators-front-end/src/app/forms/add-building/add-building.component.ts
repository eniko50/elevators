import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Building } from 'src/app/models/building.model';
import { BuildingsService } from 'src/app/service/buildings.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-building',
  templateUrl: './add-building.component.html',
  styleUrls: ['./add-building.component.css']
})
export class AddBuildingComponent implements OnInit {


  @Output()
  buildingAdded : EventEmitter<Building> = new EventEmitter();

  buildingToAdd : Building;

  constructor(private buildingsService: BuildingsService,
    private router: Router) { }

  ngOnInit() {
    this.resetBuildingToAdd();
  }

  // addBuilding() {
  //   this.buildingAdded.emit(this.buildingToAdd);
  //   this.resetBuildingToAdd();
  // }

  addBuilding(building: Building) {
    this.buildingsService.addBuilding(building).subscribe(
      (res: any) => {
        // this.loadData();

        this.resetBuildingToAdd();
        this.router.navigate(["/buildings"])
      }
    )
  }

  resetBuildingToAdd(){
    this.buildingToAdd = new Building({
      name: '',
      floors: 0
    })
  }


}

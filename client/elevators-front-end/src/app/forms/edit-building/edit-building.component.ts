import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { BuildingsService } from 'src/app/service/buildings.service';
import { Building } from 'src/app/models/building.model';

@Component({
  selector: 'app-edit-building',
  templateUrl: './edit-building.component.html',
  styleUrls: ['./edit-building.component.css']
})
export class EditBuildingComponent implements OnInit {
  
  @Input()
  buildingToEdit: Building;

  @Output()
  buildingEdited: EventEmitter<Building> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  editBuilding() {
    this.buildingEdited.emit(this.buildingToEdit);
  }

}

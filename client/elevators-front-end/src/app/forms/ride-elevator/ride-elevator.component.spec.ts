import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RideElevatorComponent } from './ride-elevator.component';

describe('RideElevatorComponent', () => {
  let component: RideElevatorComponent;
  let fixture: ComponentFixture<RideElevatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RideElevatorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RideElevatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

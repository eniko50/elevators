import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Elevator } from 'src/app/models/elevator.model';
import { RideElevator } from 'src/app/models/rideElevator.model';

@Component({
  selector: 'app-ride-elevator',
  templateUrl: './ride-elevator.component.html',
  styleUrls: ['./ride-elevator.component.css']
})
export class RideElevatorComponent implements OnInit {

  @Output()
  elevatorRided: EventEmitter<RideElevator> = new EventEmitter();

  private elevatorToRide: RideElevator;

  @Input()
  buildingId: number;

  constructor() { }

  ngOnInit() {
    this.resetRideElevatorForm();
  }

  rideElevator() {
    this.elevatorToRide.buildingId = this.buildingId;
    this.elevatorRided.emit(this.elevatorToRide);
    this.resetRideElevatorForm();
  }

  resetRideElevatorForm() {
    this.elevatorToRide = new RideElevator({
      buildingId: 0,
      startFloor: 0,
      endFloor: 0
    });
  }
}

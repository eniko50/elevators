import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditElevatorComponent } from './edit-elevator.component';

describe('EditElevatorComponent', () => {
  let component: EditElevatorComponent;
  let fixture: ComponentFixture<EditElevatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditElevatorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditElevatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

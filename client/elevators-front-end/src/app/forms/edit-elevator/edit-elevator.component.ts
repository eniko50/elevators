import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Elevator } from 'src/app/models/elevator.model';

@Component({
  selector: 'app-edit-elevator',
  templateUrl: './edit-elevator.component.html',
  styleUrls: ['./edit-elevator.component.css']
})
export class EditElevatorComponent implements OnInit {

  @Input()
  isAdministrator: boolean;
  
  @Input()
  elevatorToEdit: Elevator;

  @Output()
  elevatorEdited: EventEmitter<Elevator> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  editElevator() {
    this.elevatorEdited.emit(this.elevatorToEdit);
  }

}

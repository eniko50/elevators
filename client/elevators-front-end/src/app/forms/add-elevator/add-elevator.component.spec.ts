import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddElevatorComponent } from './add-elevator.component';

describe('AddElevatorComponent', () => {
  let component: AddElevatorComponent;
  let fixture: ComponentFixture<AddElevatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddElevatorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddElevatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

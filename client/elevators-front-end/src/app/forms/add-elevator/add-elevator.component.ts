import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { Elevator } from 'src/app/models/elevator.model';
import { Router, ActivatedRoute } from '@angular/router';
import { ElevatorService } from 'src/app/service/elevator.service';

@Component({
  selector: 'app-add-elevator',
  templateUrl: './add-elevator.component.html',
  styleUrls: ['./add-elevator.component.css']
})
export class AddElevatorComponent implements OnInit {

  @Output()
  elevatorAdded: EventEmitter<Elevator> = new EventEmitter();

  private elevatorToAdd : Elevator;
  private id: number;


  constructor(private elevatorService: ElevatorService,
    private router: Router,
    private route: ActivatedRoute) { }

  ngOnInit() {
    this.resetElevatorToAdd();
  }

  addElevator(elevator: Elevator) {
    this.route.params.subscribe(param => {
      this.id = param.id;
      this.elevatorService.addElevator(elevator, this.id)
        .subscribe((res: any) => {
          this.resetElevatorToAdd();
          this.router.navigate([`/buildings/${this.id}/elevators`]);
        });
    })
  }

  resetElevatorToAdd(){
    this.elevatorToAdd = new Elevator({
      id: 0,
      code: 0,
      currentFloor:0,
      floorsLeft:0,
      building: null,
      working: false
    });
  }
}

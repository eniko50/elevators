import { Component } from '@angular/core';
import { AuthenticationService } from './service/security/authentication.service';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'liftovi';

  constructor(private authenticaionService: AuthenticationService,
    private router: Router,
    private location: Location) { }

  isLoggedIn(): boolean {
   return this.authenticaionService.isLoggedIn();
  }

  logOut(): void {
    this.authenticaionService.logOut();
    this.router.navigate(['/login']);
  }
}

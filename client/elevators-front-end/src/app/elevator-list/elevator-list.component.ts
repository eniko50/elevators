import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { Elevator } from '../models/elevator.model';
import { RideElevator } from '../models/rideElevator.model';

@Component({
  selector: 'app-elevator-list',
  templateUrl: './elevator-list.component.html',
  styleUrls: ['./elevator-list.component.css']
})
export class ElevatorListComponent implements OnInit {
  
  @Input()
  public isAdministrator: boolean;
  
  @Input()
  public elevators : Elevator [];

  @Output()
  markElevatorForEditing : EventEmitter<Elevator> = new EventEmitter();

  @Output()
  elevatorToDelete: EventEmitter<number> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  editElevator(elevator : Elevator){
    this.markElevatorForEditing.emit(elevator);
  }

  deleteElevator(id: number) {
    this.elevatorToDelete.emit(id);
  }

}

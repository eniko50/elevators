import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BuildingListComponent } from './building-list/building-list.component';
import { BuildingPageComponent } from './pages/building-page/building-page.component';
import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component';
import { EditBuildingComponent } from './forms/edit-building/edit-building.component';
import {FormsModule} from '@angular/forms';
import { AddBuildingComponent } from './forms/add-building/add-building.component';
import { ElevatorPageComponent } from './pages/elevator-page/elevator-page.component';
import { ElevatorListComponent } from './elevator-list/elevator-list.component';
import { AddElevatorComponent } from './forms/add-elevator/add-elevator.component';
import { EditElevatorComponent } from './forms/edit-elevator/edit-elevator.component';
import { RideElevatorComponent } from './forms/ride-elevator/ride-elevator.component';
import { LoginComponent } from './login/login.component';
import { TokenInterceptorService } from './service/security/token-interceptor.service';
import { AuthenticationService } from './service/security/authentication.service';
import { CanActivateAuthGuardGuard } from './service/security/can-activate-auth-guard.guard';
import { JwtUtilsService } from './service/security/jwt-utils.service';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { AboutPageComponent } from './pages/about-page/about-page.component';
import { RegisterComponent } from './register/register.component';
import { SearchComponent } from './search/building/search.component';
import { FilterComponent } from './search/elevator/filter/filter.component';
import { CommentListComponent } from './comment-list/comment-list.component';

@NgModule({
  declarations: [
    AppComponent,
    BuildingListComponent,
    BuildingPageComponent,
    PageNotFoundComponent,
    EditBuildingComponent,
    AddBuildingComponent,
    ElevatorPageComponent,
    ElevatorListComponent,
    AddElevatorComponent,
    EditElevatorComponent,
    RideElevatorComponent,
    LoginComponent,
    HomePageComponent,
    AboutPageComponent,
    RegisterComponent,
    SearchComponent,
    FilterComponent,
    CommentListComponent
    ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
  ],
  providers: [
    {
    provide: HTTP_INTERCEPTORS,
    useClass: TokenInterceptorService,
    multi: true
  },
  AuthenticationService,
  CanActivateAuthGuardGuard,
  JwtUtilsService
],
  bootstrap: [AppComponent]
})
export class AppModule { }
